"use strict"

const os 			= require('os')
const fs 	      	= require('fs')
const path       	= require('path')
const util 			= require('util')
const express       = require('express')
const group 		= require('express-group-routes')
const app           = express()
const cors 			= require('cors')
const bodyParser    = require('body-parser')
const cookieParser  = require('cookie-parser')
const session       = require('express-session')
const compression 	= require('compression')
const multer        = require('multer')
const socket        = require('socket.io')
const ss 			= require('socket.io-stream')
const toArray 		= require('stream-to-array')
const delivery 		= require('delivery')
const program       = require('commander')
const moment 		= require('moment')
const chalk 		= require('chalk')
const define 		= require('./utils/define')
const excel 		= require('./utils/excel')
const auth 			= require('./middlewares/jwt')
const logger 		= require('./middlewares/logger')

define('PUBLIC_DIR', path.join(__dirname, 'public'))

program
	.version('2.0.0')
	.description(chalk.bold.white(`MagicalPlatform Server\n     Carlos Eduardo (carlosedba@outlook.com)`))
	.option('-p, --port [port]', 'port to listen for', 2000)
	.option('-m, --mode [mode]', 'what mode the server should run [prod|dev]', 'dev')
	.parse(process.argv)

const log = console.log
const info = console.info
const error = console.error

const host = process.env.HOST || 'localhost'
const port = process.env.PORT || program.port
const mode = program.mode
const ip = getLocalIP()

define('SERVER_MODE', mode)

switch (mode) {
	case 'dev':
		break
	case 'prod':
		break
}

app.use(compression())
app.use(express.static(global.PUBLIC_DIR))
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser('magicalcookie'))
app.use(session({
	secret: 'magicalsession',
	cookie: {
		path: '/',
		httpOnly: true,
		secure: false,
		maxAge: null
	},
	rolling: false,
	resave: false,
	proxy: undefined,
	saveUninitialized: false,
	unset: 'keep'
}))
app.use(auth)
if (mode === 'dev') app.use(logger)

const routes = require('./routes')

app.group('/', (router) => {
	routes.root.forEach((item, ind, arr) => {
		router.group(item.path, item.handler)
	})
})

app.group('/api/v1', (router) => {
	routes.api.v1.forEach((item, ind, arr) => {
		router.group(item.path, item.handler)
	})
})

app.get('*', (req, res) => {
	res.sendFile(path.join(global.PUBLIC_DIR, 'index.html'))
})


const server = app.listen(port, function () {
	if (mode === 'prod') {	
		log(`${chalk.bold.white(`/* ******************************************************\\`)}`)
		log(`${chalk.bold.white(`/* Sistema de controle de Avaliações de Satisfação  *****\\`)}`)
		log(`${chalk.bold.white(`/* Desenvolvido por Carlos Eduardo                    ***\\`)}`)
		log(`${chalk.bold.white(`/* carlosedba@outlook.com                              **\\`)}`)
		log(`${chalk.bold.white(`/* ******************************************************\\`)}`)
		log(`${chalk.bold.white(`/* Site online em: `)}${chalk.bold.green(`http://${ip}:${port}`)}`)
		log(`${chalk.bold.white(`/* ******************************************************\\`)}`)
	} else {
		log(`${chalk.bold.white(`[${moment().format('DD/MM/YYYY H:MM:SS')}]`)}${chalk.bold.yellow('[express]')}${chalk.bold.green(`[INFO]`)}${chalk.bold.white(`[Server listening on port ${port}]`)}`)
	}
})


function getLocalIP() {
	let ip = ''
	let interfaces = os.networkInterfaces()

	Object.keys(interfaces).forEach(function (iname) {
		let alias = 0

		interfaces[iname].forEach(function (iface) {
			if ('IPv4' !== iface.family || iface.internal !== false) {
				// skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
				return;
			}

			if (typeof iface.address !== undefined) {
				ip = iface.address
			}

			/*
			if (alias >= 1) {
				// this single interface has multiple ipv4 addresses
				console.log(iname + ':' + alias, iface.address);
			} else {
				// this interface has only one ipv4 adress
				console.log(iname, iface.address);
			}
			*/

			++alias
		})
	})

	return ip
}

function toBuffer(stream) {
	return new Promise((resolve, reject) => {
		toArray(stream)
			.then(function (parts) {
				const buffers = parts
					.map(part => util.isBuffer(part) ? part : Buffer.from(part));
				resolve(Buffer.concat(buffers))
			})
	})
}


const io = socket(server, { log: false })

io.on('connection', function (server) {

	ss(server).on('cl_xlsx_alunos', (stream, data) => {
		toBuffer(stream)
			.then((buffer) => {
				let alunos = excel.readAlunos(buffer)
				server.emit('sv_xlsx_alunos', alunos)
			})
	})

	/*
	let dl = delivery.listen(server)
	
	dl.on('delivery.connect', (dl) => {
		dl.on('receive.success', function (file) {
			if (file.buffer.length > 0) {
				let alunos = excel.readAlunos(file.buffer)
				server.emit('sv_xlsx', alunos)
			}
		})
	})
	*/
})
