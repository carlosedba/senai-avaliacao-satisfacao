"use strict"

module.exports = (db, DataTypes) => {
	let Curso = db.define('curso', {
		id: 				{ type: DataTypes.STRING, primaryKey: true },
		nome: 				{ type: DataTypes.STRING },
		modulos: 			{ type: DataTypes.INTEGER }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'cursos',

		classMethods: {
			associate: function (models) {
				Curso.hasMany(models.turma, { foreignKey: 'id_curso' })
				Curso.hasMany(models.uc, { foreignKey: 'id_curso' })
			}
		}
	})

	return Curso
}

