"use strict"

module.exports = (db, DataTypes) => {
	var Docente = db.define('docente', {
		id: 			{ type: DataTypes.STRING, primaryKey: true },
		nome: 			{ type: DataTypes.STRING },
		sobrenome: 		{ type: DataTypes.STRING },
		rg: 			{ type: DataTypes.STRING },
		cpf: 			{ type: DataTypes.STRING },
		cnh: 			{ type: DataTypes.STRING },
		nascimento:		{ type: DataTypes.DATE },
		cep: 			{ type: DataTypes.STRING },
		endereco: 		{ type: DataTypes.STRING },
		telefone: 		{ type: DataTypes.STRING },
		data_admissao: 	{ type: DataTypes.DATE },
		data_demissao: 	{ type: DataTypes.DATE }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'docentes',

		classMethods: {
			associate: function (models) {
				Docente.belongsToMany(models.turma, { through: { model: models.dtuc, unique: false }, foreignKey: 'id_docente', otherKey: 'id_turma' })
				Docente.belongsToMany(models.uc, { through: { model: models.dtuc, unique: false }, foreignKey: 'id_docente', otherKey: 'id_uc' })
			}
		}
	})

	return Docente
}

