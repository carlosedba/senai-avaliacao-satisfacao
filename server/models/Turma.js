"use strict"

module.exports = (db, DataTypes) => {
	let Turma = db.define('turma', {
		id: 				{ type: DataTypes.STRING, primaryKey: true },
		id_curso: 			{ type: DataTypes.STRING },
		nome: 				{ type: DataTypes.STRING },
		data_inicio: 		{ type: DataTypes.DATE },
		data_conclusao: 	{ type: DataTypes.DATE }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'turmas',

		classMethods: {
			associate: function (models) {
				Turma.belongsTo(models.curso, { foreignKey: 'id_curso' })
				Turma.belongsToMany(models.aluno, { through: { model: models.aluno_turma, unique: false }, foreignKey: 'id_turma', otherKey: 'id_aluno' })
				Turma.belongsToMany(models.docente, { through: { model: models.dtuc, unique: false }, foreignKey: 'id_turma', otherKey: 'id_docente' })
			}
		}
	})

	return Turma
}

