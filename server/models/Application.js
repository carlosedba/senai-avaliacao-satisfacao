"use strict"

module.exports = (db, DataTypes) => {
	var Application = db.define('application', {
		id: 			{ type: DataTypes.STRING, primaryKey: true },
		name: 			{ type: DataTypes.STRING },
		password: 		{ type: DataTypes.STRING },
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'applications',
	})

	return Application
}

