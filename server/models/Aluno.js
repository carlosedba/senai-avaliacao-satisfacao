"use strict"

module.exports = (db, DataTypes) => {
	var Aluno = db.define('aluno', {
		id: 			{ type: DataTypes.STRING, primaryKey: true, allowNull: false },
		matricula: 		{ type: DataTypes.INTEGER },
		nome: 			{ type: DataTypes.STRING },
		cpf: 			{ type: DataTypes.STRING },
		nascimento:		{ type: DataTypes.DATE },
		situacao: 		{ type: DataTypes.STRING }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'alunos',

		classMethods: {
			associate: function (models) {
				Aluno.belongsToMany(models.turma, { through: { model: models.aluno_turma, unique: false }, foreignKey: 'id_aluno', otherKey: 'id_turma' })
				Aluno.hasMany(models.avaliacao_satisfacao, { foreignKey: 'id_aluno' })
			}
		}
	})

	return Aluno
}

