'use strict'

var fs 				= require('fs')
var path 			= require('path')
var Sequelize 		= require('sequelize')
var basename		= path.basename(module.filename)
var db 				= {}
var logging = (global.SERVER_MODE === 'prod') ? false : console.log

var sequelize = new Sequelize({
	host: 'localhost',
	dialect: 'sqlite',

	pool: {
		max: 5, 
		min: 0,
		idle: 10000
	},

	storage: path.join(__dirname, '../data/AvaliacaoSatisfacao.sqlite'),
	sync: true,
	logging: logging
})

fs
.readdirSync(__dirname)
.filter(function(file) {
	return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js')
})
.forEach(function(file) {
	var model = sequelize['import'](path.join(__dirname, file))
	db[model.name] = model
})

Object.keys(db).forEach(function(modelName) {
	if (db[modelName].associate) {
		db[modelName].associate(db)
	}
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
