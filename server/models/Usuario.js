"use strict"

module.exports = (db, DataTypes) => {
	var Usuario = db.define('usuario', {
		id: 			{ type: DataTypes.STRING, primaryKey: true },
		nome: 			{ type: DataTypes.STRING },
		email: 			{ type: DataTypes.STRING },
		senha: 			{ type: DataTypes.STRING },
		permissoes:		{ type: DataTypes.STRING },
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'usuarios',
	})

	return Usuario
}

