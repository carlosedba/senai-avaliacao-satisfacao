"use strict"

module.exports = (db, DataTypes) => {
	var AvaliacaoSatisfacao = db.define('avaliacao_satisfacao', {
		id: 			{ type: DataTypes.STRING, primaryKey: true },
		id_aluno: 		{ type: DataTypes.STRING },
		id_dtuc: 		{ type: DataTypes.STRING },
		resultado: 		{ type: DataTypes.STRING }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'avaliacoes_satisfacao',

		classMethods: {
			associate: function (models) {
				AvaliacaoSatisfacao.belongsTo(models.aluno, { foreignKey: 'id_aluno' })
				AvaliacaoSatisfacao.belongsTo(models.dtuc, { foreignKey: 'id_dtuc' })
			}
		}
	})

	return AvaliacaoSatisfacao
}

