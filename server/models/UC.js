"use strict"

module.exports = (db, DataTypes) => {
	let UC = db.define('uc', {
		id: 				{ type: DataTypes.STRING, primaryKey: true },
		id_curso: 			{ type: DataTypes.STRING },
		nome: 				{ type: DataTypes.STRING },
		carga_horaria: 		{ type: DataTypes.INTEGER }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'ucs',

		classMethods: {
			associate: function (models) {
				UC.belongsTo(models.curso, { foreignKey: 'id_curso' })
				UC.belongsToMany(models.docente, { through: { model: models.dtuc, unique: false }, foreignKey: 'id_uc', otherKey: 'id_docente' })
			}
		}
	})

	return UC
}

