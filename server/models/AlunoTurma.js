"use strict"

module.exports = (db, DataTypes) => {
	let AlunoTurma = db.define('aluno_turma', {
		id: 			{ type: DataTypes.STRING, primaryKey: true },
		id_aluno: 		{ type: DataTypes.STRING },
		id_turma: 		{ type: DataTypes.STRING }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'alunos_turma',

		classMethods: {
			associate: function (models) {
				AlunoTurma.belongsTo(models.aluno, { foreignKey: 'id_aluno' })
				AlunoTurma.belongsTo(models.turma, { foreignKey: 'id_turma' })
			}
		}
	})

	return AlunoTurma
}

