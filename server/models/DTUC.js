"use strict"

module.exports = (db, DataTypes) => {
	let DocenteTurmaUC = db.define('dtuc', {
		id: 				{ type: DataTypes.STRING, primaryKey: true },
		id_docente:			{ type: DataTypes.STRING },
		id_turma: 			{ type: DataTypes.STRING },
		id_uc: 				{ type: DataTypes.STRING },
		modulo: 			{ type: DataTypes.INTEGER }
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'dtucs',

		classMethods: {
			associate: function (models) {
				DocenteTurmaUC.belongsTo(models.docente, { foreignKey: 'id_docente' })
				DocenteTurmaUC.belongsTo(models.turma, { foreignKey: 'id_turma' })
				DocenteTurmaUC.belongsTo(models.uc, { foreignKey: 'id_uc' })
				DocenteTurmaUC.hasMany(models.avaliacao_satisfacao, { foreignKey: 'id_dtuc' })
			}
		}
	})

	return DocenteTurmaUC
}

