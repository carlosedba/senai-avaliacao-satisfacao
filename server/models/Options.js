"use strict"

module.exports = (db, DataTypes) => {
	var Option = db.define('option', {
		name: 			{ type: DataTypes.STRING, primaryKey: true },
		value: 			{ type: DataTypes.STRING },
	}, {
		timestamps: 		true,
		paranoid: 			false,
		underscored: 		true,
		freezeTableName: 	true,
		tableName: 			'options',
	})

	return Option
}

