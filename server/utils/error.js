const errors = [
	{ code: 5000, 	message: 'No authorization token was found.' },
	{ code: 5001, 	message: 'A invalid token was provided.' },
	{ code: 5002, 	message: 'Password does not match.' },
	{ code: 5003, 	message: 'Email not found.' },
	{ code: 5004, 	message: 'Application not found.' },
]

module.exports = function (code) {
	let error = null

	errors.forEach(function (el, ind, arr) {
		if (el.code === code) {
			error = el
		}
	})

	return error
}