const moment = require('moment')

module.exports = {
	convertDateValue(datevalue) {
		datevalue -= 2
		const ONE_DAY = 24 * 60 * 60 * 1000
		const BASE = moment("01/01/1900", "DD/MM/YYYY")

		let milliseconds = Math.round(Math.abs((datevalue * ONE_DAY) + BASE._d.getTime()))
		let date = new Date(milliseconds)

		return date
	}
}