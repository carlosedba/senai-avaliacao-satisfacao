const xlsx = require('node-xlsx')
const math = require('./math')

module.exports = {
	readAlunos(file) {
		let worksheets = xlsx.parse(file)
		let length = worksheets[0].data.length
		let data = worksheets[0].data
		let alunos = []

		for (var i = 15; i < length; i++) {
			if (data[i].length > 16) {
				let aluno = {
					matricula: 		data[i][9],
					nome: 			data[i][0],
					cpf: 			data[i][13],
					nascimento: 	math.convertDateValue(data[i][5]),
					situacao: 		data[i][16]
				}

				alunos.push(aluno)
			}
		}

		return alunos
	}
}