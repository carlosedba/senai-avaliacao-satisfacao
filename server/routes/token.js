"use strict"

const fs 			= require('fs')
const path 			= require('path')
const jwt 			= require('jsonwebtoken')
const crypto 		= require('../security/crypto-pbkdf2-promise')
const models 		= require('../models')
const error 		= require('../utils/error')

const privateKey 	= fs.readFileSync(path.join(__dirname, '../security/private_key.pem'))
const publicKey 	= fs.readFileSync(path.join(__dirname, '../security/public_key.pub'))


function generateToken(instance) {
	let payload = {
		id: 			instance.id,
		nome: 			instance.nome,
		email: 			instance.email,
		permissoes: 	instance.permissoes,
	}

	let options = {
		algorithm: 		'RS256',
		expiresIn: 		Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 7),
		//notBefore: 		`${Math.floor(Date.now() - 6000)}`,
		issuer: 		'http://avaliacao-satisfacao.herokuapp.com',
	}

	return jwt.sign(payload, privateKey, options)
}

function verifyToken(token, callback) {
	let options = {
		algorithms: 		['RS256', 'RS384', 'RS512'],
		issuer: 			'http://avaliacao-satisfacao.herokuapp.com',
		ignoreExpiration: 	false,
		ignoreNotBefore: 	false,
	}

	jwt.verify(token, publicKey, options, callback)
}

function token(req, res) {
	let email = req.body.email
	let senha = req.body.senha

	/*crypto.hashPassword(senha)
		.then((hash) => console.log(hash))*/

	return models.usuario.findOne({
		where: {
			email: email,
		}
	})
	.then((instance) => {
		crypto.verifyPassword(senha, instance.senha)
			.then((isCorrect) => {
				res.json({ token: generateToken(instance) })
			})
			.catch((err) => {
				res.json({ error: error(5002) })
			})
	})
	.catch((err) => {
		res.json({ error: error(5003) })
	})
}

function renew(req, res) {
	let token = req.get('X-Access-Token')

	if (token) {
		verifyToken(token, (err, valid) => {
			if (err) res.status(401).json({ error: error(5001) })
			res.json({ token: generateToken(valid) })
		})
	} else {
		res.status(401).json({ error: error(5000) })
	}
}

module.exports = (router) => {
	router.route('/')
		.post(token)

	router.route('/renew')
		.get(renew)
}

