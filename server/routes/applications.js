"use strict"

const fs 			= require('fs')
const path 			= require('path')
const jwt 			= require('jsonwebtoken')
const crypto 		= require('../security/crypto-pbkdf2-promise')
const models 		= require('../models')
const error 		= require('../utils/error')

const privateKey 	= fs.readFileSync(path.join(__dirname, '../security/private_key.pem'))

function generateToken(instance) {
	let payload = {
		iat: 			Math.floor(Date.now() / 1000),
		id: 			instance.id,
	}

	let options = {
		algorithm: 		'RS256',
		expiresIn: 		Math.floor(Date.now() / 1000) + (60 * 60 * 24),
		notBefore: 		Math.floor(Date.now() / 1000),
		issuer: 		'http://avaliacao-satisfacao.herokuapp.com',
	}

	return jwt.sign(payload, privateKey, options)
}

function token(req, res) {
	let name = req.body.name
	let password = req.body.password

	crypto.hashPassword(password)
		.then((hash) => console.log(hash))

	return models.application.findOne({
		where: {
			name: name,
		}
	})
	.then((instance) => {
		crypto.verifyPassword(password, instance.password)
			.then((isCorrect) => {
				res.json({ token: generateToken(instance) })
			})
			.catch((err) => {
				res.json({ error: error(5002) })
			})
	})
	.catch((err) => {
		res.json({ error: error(5004) })
	})
}

module.exports = (router) => {
	router.route('/')
		.post(token)
}

