"use strict"

let models = require('../models')


module.exports = (router) => {
	router.route('/')
		.get((req, res) => {
			models.application.sync({ force: true })
			models.option.sync({ force: true })
			models.curso.sync({ force: true })
			models.turma.sync({ force: true })
			models.aluno.sync({ force: true })
			models.docente.sync({ force: true })
			models.uc.sync({ force: true })
			models.usuario.sync({ force: true })
			models.avaliacao_satisfacao.sync({ force: true })
			models.aluno_turma.sync({ force: true })
			models.dtuc.sync({ force: true })
				.then(() => {
					models.application.create({
						name: 'AvaliacaoSatisfacao',
						value: 'AAAAEAAAw1D7FrGiagCvH0PFPXBstEEMYbTE/lumj7SBLWgvZW0agd/6/NS2r5l+LP61rKDS/B4=', //I10y644^h
					})
				})
				.then(() => {
					models.option.create({
						name: 'index_satisfacao',
						value: 'true',
					})
				})
				.then(() => {
					models.usuario.create({
						id: '1',
						nome: 'Carlos Eduardo',
						email: 'carlosedba@outlook.com',
						senha: 'AAAAEAAAw1Ah+Pz/79dVFmm+ba10ViBJcAn0keSBwbhXAykFoTiNt0VGjyBpM9GsXsm6MWVII+A=',
						permissoes: '',
					})
					models.curso.create({
						id: '1',
						nome: 'Técnico em Informática',
						modulos: '3',
					})
					models.curso.create({
						id: '2',
						nome: 'Técnico em Mecânica',
						modulos: '4',
					})
					models.curso.create({
						id: '3',
						nome: 'Técnico em Logística',
						modulos: '4',
					})
				})
				.then(() => {
					models.turma.create({
						id: '1',
						id_curso: '1',
						nome: 'TEC-INFO-01',
						data_inicio: '19/12/2016',
						data_conclusao: '31/12/2016',
					})
				})
				.then(() => {
					models.uc.create({
						id: '1',
						id_curso: '1',
						nome: 'Técnicas de Programação',
					})
				})
				.then(() => {
					models.docente.create({
						id: '1',
						nome: 'Luiz Carlos',
						cpf: '566.637.731-00',
						nascimento: '16/07/1955',
					})
				})
				.then(() => {
					models.aluno.create({
						id: '1',
						matricula: '12345678',
						nome: 'Carlos Eduardo Barbosa de Almeida',
						cpf: '655.797.920-50',
						nascimento: '15/01/1999',
					})
				})
				.then(() => {
					models.aluno_turma.create({
						id: '1',
						id_aluno: '1',
						id_turma: '1',
					})
				})
				.then(() => {
					models.dtuc.create({
						id: '1',
						id_docente: '1',
						id_turma: '1',
						id_uc: '1',
					})
				})
				.then(() => {
					return res.sendStatus(200)
				})
		})
}

