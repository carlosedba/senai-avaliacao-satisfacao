"use strict"

let models = require('../models')

function getAlunos(req, res) {
	return models.aluno.findAll({
		include: [{
			model: models.turma,
			through: models.aluno_turma,
			include: [{
				model: models.curso
			}]
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getAluno(req, res) {
	let id = req.params.id

	return models.aluno.findById(id, {
		include: [{
			model: models.turma,
			through: models.aluno_turma,
			include: [{
				model: models.curso
			}]
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getAlunoNome(req, res) {
	let query = req.query.q

	return models.aluno.findOne({
		attributes: ['id', 'nome'],
		where: {
			$or: [
				{nome: query},
			]
		}
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createAluno(req, res) {
	let values = Object.assign({}, req.body)

	return models.aluno.create(values)
		.then((instance) => {
			res.send(instance)
		})
		.catch((err) => {
			res.json({
				error: 5006,
				message: `${err.name} - ${err.message}`
			})
		})
}

function updateAluno(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)
	
	return models.aluno.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteAluno(req, res) {
	let id = req.params.id
	
	return models.aluno.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function searchAlunos(req, res) {
	let query = req.query.q.toLowerCase()

	return models.aluno.findAll({
		attributes: ['id', 'nome'],
		where: {
			$or: [
				{nome: 			{ $like: `%${query}%` }},
			]
		}
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getAlunosTurma(req, res) {
	let turma = (req.query.turma) ? req.query.turma.toLowerCase() : false
	turma = (turma) ? { where: { id_turma: turma } } : {}

	let options = Object.assign({}, turma, {
		include: [{
			model: models.aluno
		}, {
			model: models.turma
		}]
	})

	return models.aluno_turma.findAll(options)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getAlunoTurma(req, res) {
	let id = req.params.id
	
	return models.aluno_turma.findById(id)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createAlunoTurma(req, res) {
	let values = Object.assign({}, req.body)
	console.log(values)

	return models.aluno_turma.create(values, { fields: ['id', 'id_aluno', 'id_turma'] })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateAlunoTurma(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)

	return models.aluno_turma.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteAlunoTurma(req, res) {
	let id = req.params.id

	return models.aluno_turma.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/turmas')
		.get(getAlunosTurma)
		.post(createAlunoTurma)

	router.route('/turmas/:id')
		.get(getAlunoTurma)
		.put(updateAlunoTurma)
		.delete(deleteAlunoTurma)

	router.route('/search')
		.get(searchAlunos)

	router.route('/nome')
		.get(getAlunoNome)

	router.route('/')
		.get(getAlunos)
		.post(createAluno)

	router.route('/:id')
		.get(getAluno)
		.put(updateAluno)
		.delete(deleteAluno)
}

