"use strict"

let models = require('../models')

function getSatisfacoes(req, res) {
	let dtuc = (req.query.dtuc) ? req.query.dtuc.toLowerCase() : false
	dtuc = (dtuc) ? { where: { id_dtuc: dtuc } } : {}

	let aluno = (req.query.aluno) ? req.query.aluno.toLowerCase() : false
	aluno = (aluno) ? { where: { id_aluno: aluno } } : {}

	let options = Object.assign({}, dtuc, aluno, {
		include: [{
			model: models.aluno
		}, {
			model: models.dtuc
		}]
	})

	return models.avaliacao_satisfacao.findAll(options)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getSatisfacao(req, res) {
	let id = req.params.id

	return models.avaliacao_satisfacao.findById(id, {
		include: [{
			model: models.aluno
		}, {
			model: models.dtuc
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}


function createSatisfacao(req, res) {
	let values = Object.assign({}, req.body)
	
	return models.avaliacao_satisfacao.create(values)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateSatisfacao(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)
	
	return models.avaliacao_satisfacao.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteSatisfacao(req, res) {
	let id = req.params.id

	return models.avaliacao_satisfacao.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteSatisfacaoByDTUC(req, res) {
	let dtuc = req.query.dtuc

	return models.avaliacao_satisfacao.destroy({ where: { id_dtuc: dtuc } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/')
		.get(getSatisfacoes)
		.post(createSatisfacao)

	router.route('/delete')
		.delete(deleteSatisfacaoByDTUC)

	router.route('/:id')
		.get(getSatisfacao)
		.put(updateSatisfacao)
		.delete(deleteSatisfacao)
}

