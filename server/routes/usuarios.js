"use strict"

const crypto = require('../security/crypto-pbkdf2-promise')
let models = require('../models')

function getUsuarios(req, res) {
	return models.usuario.findAll()
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getUsuario(req, res) {
	let id = req.params.id

	return models.usuario.findById(id)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createUsuario(req, res) {
	let values = Object.assign({}, req.body)

	crypto.hashPassword(values.senha)
		.then((hash) => {
			values.senha = hash
		})
		.then(() => {
			models.usuario.create(values)
				.then((instance) => {
					res.send(instance)
				})
				.catch((err) => {
					res.json({
						error: 5007,
						message: `${err.name} - ${err.message}`
					})
				})
		})
}

function updateUsuario(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)

	crypto.hashPassword(values.senha)
		.then((hash) => {
			values.senha = hash
		})
		.then(() => {
			models.usuario.update(values, { where: { id: id } })
				.then((instance) => {
					res.send(instance)
				})
				.catch((err) => {
					res.json({
						error: 5007,
						message: `${err.name} - ${err.message}`
					})
				})
		})
}

function deleteUsuario(req, res) {
	let id = req.params.id

	return models.usuario.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/')
		.get(getUsuarios)
		.post(createUsuario)

	router.route('/:id')
		.get(getUsuario)
		.put(updateUsuario)
		.delete(deleteUsuario)
}

