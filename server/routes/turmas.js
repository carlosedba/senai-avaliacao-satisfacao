"use strict"

let models = require('../models')

function getTurmas(req, res) {
	return models.turma.findAll({
		include: [{
			model: models.curso
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getTurma(req, res) {
	let id = req.params.id

	return models.turma.findById(id, {
		include: [{
			model: models.curso
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createTurma(req, res) {
	let values = Object.assign({}, req.body)
	
	return models.turma.create(values)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateTurma(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)
	
	return models.turma.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteTurma(req, res) {
	let id = req.params.id

	return models.turma.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function searchTurmas(req, res) {
	let query = req.query.q.toLowerCase()

	return models.turma.findAll({
		where: {
			$or: [
				{nome: 			{ $like: `%${query}%` }},
			]
		}
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/search')
		.get(searchTurmas)

	router.route('/')
		.get(getTurmas)
		.post(createTurma)

	router.route('/:id')
		.get(getTurma)
		.put(updateTurma)
		.delete(deleteTurma)
}

