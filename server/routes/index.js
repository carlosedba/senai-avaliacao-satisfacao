const api 				= require('./api')
const applications 		= require('./applications')
const token 			= require('./token')
const all 				= require('./all')
const setup 			= require('./setup')
const alunos 			= require('./alunos')
const cursos 			= require('./cursos')
const docentes 			= require('./docentes')
const options 			= require('./options')
const satisfacao 		= require('./satisfacao')
const search	 		= require('./search')
const turmas 			= require('./turmas')
const ucs 				= require('./ucs')
const usuarios 			= require('./usuarios')

module.exports = {
	root: [
		{ path: '/api', 				handler: api },
	],

	api: {
		v1: [
			{ path: '/', 				handler: all },
			{ path: '/applications',	handler: applications },
			{ path: '/token', 			handler: token },
			{ path: '/setup', 			handler: setup },
			{ path: '/alunos', 			handler: alunos },
			{ path: '/cursos', 			handler: cursos },
			{ path: '/docentes', 		handler: docentes },
			{ path: '/options', 		handler: options },
			{ path: '/satisfacao',		handler: satisfacao },
			{ path: '/search',			handler: search },
			{ path: '/turmas',			handler: turmas },
			{ path: '/ucs',				handler: ucs },
			{ path: '/usuarios',		handler: usuarios },
		]
	}
}