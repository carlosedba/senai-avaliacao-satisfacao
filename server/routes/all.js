"use strict"

let models = require('../models')

function getAlunos() {
	return models.aluno.findAll({
		include: [{
			model: models.turma,
			through: models.aluno_turma,
			include: [{
				model: models.curso
			}]
		}]
	})
}

function getCursos() {
	return models.curso.findAll({
		include: [{
			model: models.turma
		}, {
			model: models.uc
		}]
	})
}

function getDocentes() {
	return models.docente.findAll({
		include: [{
			model: models.turma
		}, {
			model: models.uc
		}]
	})
}

function getSatisfacoes() {
	return models.avaliacao_satisfacao.findAll({
		include: [{
			model: models.aluno
		}, {
			model: models.dtuc
		}]
	})
}

function getTurmas() {
	return models.turma.findAll({
		include: [{
			model: models.curso
		}]
	})
}

function getUCs() {
	return models.uc.findAll({
		include: [{
			model: models.curso
		}]
	})
}

function getAll(req, res) {
	return Promise.all([
		getAlunos(),
		getCursos(),
		getDocentes(),
		getSatisfacoes(),
		getTurmas(),
		getUCs(),
	])
	.then((data) => {
		let all = {
			alunos: 		data[0],
			cursos: 		data[1],
			docentes: 		data[2],
			satisfacoes: 	data[3],
			turmas: 		data[4],
			ucs: 			data[5],
		}

		return res.json(all)
	})
}

module.exports = (router) => {
	router.route('/')
		.get(getAll)
}

