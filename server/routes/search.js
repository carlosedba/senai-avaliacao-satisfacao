"use strict"

let models = require('../models')

function search(req, res) {
	let calls = []
	let results = {}

	let query = req.query.q.toLowerCase()
	let include = (req.query.in) ? req.query.in.split(',') : []


	if (include.includes('alunos')) {
		calls.push(models.aluno.findAll({
			where: {
				$or: [
					{id: 			{ $like: `%${query}%` }},
					{nome: 			{ $like: `%${query}%` }},
					{matricula:		{ $like: `%${query}%` }},
					{cpf: 			{ $like: `%${query}%` }},
				]
			}
		}))
	}

	if (include.includes('docentes')) {
		calls.push(models.docente.findAll({
			where: {
				$or: [
					{id: 			{ $like: `%${query}%` }},
					{nome: 			{ $like: `%${query}%` }},
				]
			}
		}))
	}

	if (include.includes('cursos')) {
		calls.push(models.curso.findAll({
			where: {
				$or: [
					{id: 			{ $like: `%${query}%` }},
					{nome: 			{ $like: `%${query}%` }},
				]
			}
		}))
	}

	if (include.includes('turmas')) {
		calls.push(models.turma.findAll({
			where: {
				$or: [
					{id: 			{ $like: `%${query}%` }},
					{nome: 			{ $like: `%${query}%` }},
				]
			}
		}))
	}

	if (query.length > 0) {
		return Promise.all(calls)
		.then((data) => {
			data.map((el, i) => {
				if (el.length > 0) {
					switch (i) {
						case 0:
							results = Object.assign({}, results, { alunos: el })
							break
						case 1:
							results = Object.assign({}, results, { docentes: el })
							break
						case 2:
							results = Object.assign({}, results, { cursos: el })
							break
						case 3:
							results = Object.assign({}, results, { turmas: el })
							break
					}
				} else {
					switch (i) {
						case 0:
							results = Object.assign({}, results, { alunos: [] })
							break
						case 1:
							results = Object.assign({}, results, { docentes: [] })
							break
						case 2:
							results = Object.assign({}, results, { cursos: [] })
							break
						case 3:
							results = Object.assign({}, results, { turmas: [] })
							break
					}
				}
			})
		})
		.then(() => {
			res.send(results)
		})
	} else {
		res.send(null)
	}
}

module.exports = (router) => {
	router.route('/')
		.get(search)
}

