"use strict"

let models = require('../models')

function getOptions(req, res) {
	return models.option.findAll()
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getOption(req, res) {
	let name = req.params.name

	return models.option.find({ where: { name: name } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createOption(req, res) {
	let values = Object.assign({}, req.body)

	return models.option.create(values)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateOption(req, res) {
	let name = req.params.name
	let values = Object.assign({}, req.body)
	
	return models.option.update(values, { where: { name: name } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteOption(req, res) {
	let name = req.params.name

	return models.option.destroy({ where: { name: name } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/')
		.get(getOptions)
		.post(createOption)

	router.route('/:name')
		.get(getOption)
		.put(updateOption)
		.delete(deleteOption)
}

