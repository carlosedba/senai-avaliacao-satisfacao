"use strict"

let models = require('../models')

function getUCs(req, res) {
	return models.uc.findAll({
		include: [{
			model: models.curso
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getUC(req, res) {
	let id = req.params.id

	return models.uc.findById(id, {
		include: [{
			model: models.curso
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createUC(req, res) {
	let values = Object.assign({}, req.body)
	
	return models.uc.create(values)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateUC(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)
	
	return models.uc.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteUC(req, res) {
	let id = req.params.id

	return models.uc.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function searchUCs(req, res) {
	let query = req.query.q.toLowerCase()

	return models.uc.findAll({
		where: {
			$or: [
				{nome: 			{ $like: `%${query}%` }},
			]
		}
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/search')
		.get(searchUCs)

	router.route('/')
		.get(getUCs)
		.post(createUC)

	router.route('/:id')
		.get(getUC)
		.put(updateUC)
		.delete(deleteUC)
}

