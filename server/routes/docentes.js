"use strict"

let models = require('../models')

function getDocentes(req, res) {
	return models.docente.findAll({
		include: [{
			model: models.turma
		}, {
			model: models.uc
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getDocente(req, res) {
	let id = req.params.id

	return models.docente.findById(id, {
		include: [{
			model: models.turma
		}, {
			model: models.uc
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createDocente(req, res) {
	let values = Object.assign({}, req.body)
	
	return models.docente.create(values)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateDocente(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)
	
	return models.docente.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteDocente(req, res) {
	let id = req.params.id

	return models.docente.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function searchDocentes(req, res) {
	let query = req.query.q.toLowerCase()

	return models.docente.findAll({
		where: {
			$or: [
				{nome: 			{ $like: `%${query}%` }},
			]
		}
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getDTUCs(req, res) {
	let turma = (req.query.turma) ? req.query.turma.toLowerCase() : false
	turma = (turma) ? { where: { id_turma: turma } } : {}

	let options = Object.assign({}, turma, {
		include: [{
			model: models.docente
		}, {
			model: models.turma,
			include: [{
				model: models.curso
			}]
		}, {
			model: models.uc
		}, {
			model: models.avaliacao_satisfacao
		}]
	})

	return models.dtuc.findAll(options)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getDTUC(req, res) {
	let id = req.params.id

	return models.dtuc.findById(id, {
		include: [{
			model: models.docente
		}, {
			model: models.turma,
			include: [{
				model: models.curso
			}]
		}, {
			model: models.uc
		}, {
			model: models.avaliacao_satisfacao
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createDTUC(req, res) {
	let values = Object.assign({}, req.body)
	
	return models.dtuc.create(values)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateDTUC(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)
	
	return models.dtuc.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteDTUC(req, res) {
	let id = req.params.id

	return models.dtuc.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/turmas')
		.get(getDTUCs)
		.post(createDTUC)

	router.route('/turmas/:id')
		.get(getDTUC)
		.put(updateDTUC)
		.delete(deleteDTUC)

	router.route('/search')
		.get(searchDocentes)

	router.route('/')
		.get(getDocentes)
		.post(createDocente)

	router.route('/:id')
		.get(getDocente)
		.put(updateDocente)
		.delete(deleteDocente)
}

