"use strict"

let models = require('../models')

function getCursos(req, res) {
	return models.curso.findAll({
		include: [{
			model: models.turma
		}, {
			model: models.uc
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function getCurso(req, res) {
	let id = req.params.id

	return models.curso.findById(id, {
		include: [{
			model: models.turma
		}, {
			model: models.uc
		}]
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function createCurso(req, res) {
	let values = Object.assign({}, req.body)

	return models.curso.create(values)
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function updateCurso(req, res) {
	let id = req.params.id
	let values = Object.assign({}, req.body)
	
	return models.curso.update(values, { where: { id: id } })
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function deleteCurso(req, res) {
	let id = req.params.id

	return models.curso.destroy({ where: { id: id } })
	.then((instance) => {
		(instance) ? res.sendStatus(200) : res.sendStatus(404)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

function searchCursos(req, res) {
	let query = req.query.q.toLowerCase()

	return models.curso.findAll({
		where: {
			$or: [
				{nome: 			{ $like: `%${query}%` }},
			]
		}
	})
	.then((instance) => {
		res.send(instance)
	})
	.catch((err) => {
		res.json({
			error: 5007,
			message: `${err.name} - ${err.message}`
		})
	})
}

module.exports = (router) => {
	router.route('/search')
		.get(searchCursos)

	router.route('/')
		.get(getCursos)
		.post(createCurso)

	router.route('/:id')
		.get(getCurso)
		.put(updateCurso)
		.delete(deleteCurso)
}

