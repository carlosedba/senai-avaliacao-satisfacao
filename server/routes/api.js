"use strict"

const models = require('../models')

function api(req, res) {
	res.send('API')
}

module.exports = (router) => {
	router.route('/')
		.get(api)
}

