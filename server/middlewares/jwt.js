"use strict"

const fs 			= require('fs')
const path 			= require('path')
const jwt 			= require('jsonwebtoken')
const unless 		= require('express-unless')
const error 		= require('../utils/error')

const publicKey 	= fs.readFileSync(path.join(__dirname, '../security/public_key.pub'))

const conditions = {
	path: [
		/^(?!\/api).*/,
		'/api/v1/token',
		'/api/v1/setup',
		'/api/v1/options/index_satisfacao',
		/^\/api\/v1\/satisfacao\/*/,
		/^\/api\/v1\/alunos\/*/,
		/^\/api\/v1\/docentes\/turmas\/*/,
		/^\/api\/v1\/ucs\/*/,
	]
}


function isPublic(path) {
	return conditions.path.every((el, i) => {
		console.log(path, el)
		if (path === el) {
			return true
		}
	})
}

function verifyToken(token, callback) {
	let options = {
		algorithms: 		['RS256', 'RS384', 'RS512'],
		issuer: 			'http://avaliacao-satisfacao.herokuapp.com',
		ignoreExpiration: 	false,
		ignoreNotBefore: 	false,
	}

	jwt.verify(token, publicKey, options, callback)
}

function middleware(req, res, next) {
	let token = req.body.token || req.query.token || req.headers['x-access-token']

	if (token) {
		verifyToken(token, (err, valid) => {
			if (err) res.status(401).json({ error: error(5001) })
			next()
		})
	} else {
		res.status(401).json({ error: error(5000) })
	}
}

middleware.unless = unless

module.exports = middleware.unless(conditions)

