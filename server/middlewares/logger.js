"use strict"

const moment = require('moment')
const chalk = require('chalk')

const log = console.log
const info = console.info
const error = console.error

const Logger = (req, res, next) => {
	let date = moment().format('DD/MM/YYYY H:MM:SS')
	let ip = req.ip
	let url = req.originalUrl
	let status

	switch (String(res.statusCode).charAt(0)) {
		case '2':
			status = chalk.bold.green(`[${res.statusCode}]`)
			break

		case '3':
			status = chalk.bold.cyan(`[${res.statusCode}]`)
			break
		case '4':
			status = chalk.bold.red(`[${res.statusCode}]`)
			break
		case '5':
			status = chalk.bold.red(`[${res.statusCode}]`)
			break
		default:
			status = chalk.bold.white(`[${res.statusCode}]`)
	}

	log(`${chalk.bold.white(`[${date}]`)}${chalk.bold.yellow('[express]')}${status}${chalk.bold.white(`[IP: ${ip}][URL: ${url}]`)}`)

	next()
}

module.exports = Logger

