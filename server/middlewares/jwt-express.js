"use strict"

const fs 			= require('fs')
const path 			= require('path')
const jwt 			= require('express-jwt')

const publicKey = fs.readFileSync(path.join(__dirname, '../security/public_key.pub'))

const conditions = {
	path: ['/api/v1/token', '/login', '/']
}

function handleToken(err, req, res, next) {
	//if (!req.user.admin) return res.sendStatus(401)
	console.log(req)
	res.sendStatus(200)

	next()
}

function tokenNotFound(err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.status(401).send('Error 401 - No authorization token was found')
	}
}


module.exports = {
	handleToken: jwt({ secret: publicKey }, handleToken).unless(conditions),
	tokenNotFound: tokenNotFound
}

/*
const jwt = Promise.promisifyAll(require('jsonwebtoken'))

const Auth = (req, res, next) => {
	let token = req.body.token || req.param('token') || req.headers['x-access-token']

	if (token) {

	}

	next()
}

module.exports = Auth

{
	algorithm: 'HS256',
	iss: "http://ambiduos.com.br",
	expiresIn: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 7),
	notBefore: Math.floor(Date.now() / 1000)
}
{
	iat: Math.floor(Date.now() / 1000)
	id,
	email,
	nome,
	tipo
}
*/

