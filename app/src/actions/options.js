import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchOptions() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/options`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_OPTIONS,
		payload: request
	}
}




export function fetchOption(name) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/options/${name}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_OPTION,
		payload: request
	}
}





export function createOption(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/options`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_OPTION,
		payload: request
	}
}



export function updateOption(name, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/options/${name}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_OPTION,
		payload: request
	}
}




export function deleteOption(name) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/options/${name}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_OPTION,
		payload: request
	}
}