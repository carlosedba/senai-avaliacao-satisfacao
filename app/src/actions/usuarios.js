import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchUsuarios() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/usuarios`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_USUARIOS,
		payload: request
	}
}




export function fetchUsuario(usuario) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/usuarios/${usuario}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_USUARIO,
		payload: request
	}
}





export function createUsuario(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/usuarios`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_USUARIO,
		payload: request
	}
}



export function updateUsuario(usuario, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/usuarios/${usuario}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_USUARIO,
		payload: request
	}
}




export function deleteUsuario(usuario) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/usuarios/${usuario}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_USUARIO,
		payload: request
	}
}