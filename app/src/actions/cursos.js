import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchCursos() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/cursos`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_CURSOS,
		payload: request
	}
}




export function fetchCurso(curso) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/cursos/${curso}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_CURSO,
		payload: request
	}
}





export function createCurso(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/cursos`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_CURSO,
		payload: request
	}
}



export function updateCurso(curso, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/cursos/${curso}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_CURSO,
		payload: request
	}
}




export function deleteCurso(curso) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/cursos/${curso}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_CURSO,
		payload: request
	}
}