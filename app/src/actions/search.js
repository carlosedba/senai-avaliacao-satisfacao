import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function search(params) {
	let search = `?q=${params.query}`
	let include = '&in='

	if (params.include) {
		params.include.map((el) => {
			include += el + ','
		})
		include = include.replace(/,\s*$/, "")
	}

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/search${search}${include}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.SEARCH,
		payload: request
	}
}

