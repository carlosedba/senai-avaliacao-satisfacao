import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchUCs() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/ucs`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_UCS,
		payload: request
	}
}




export function fetchUC(uc) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/ucs/${uc}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_UC,
		payload: request
	}
}




export function fetchUCCurso(curso) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/ucs/curso/${curso}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_UC_CURSO,
		payload: request
	}
}





export function createUC(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/ucs`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_UC,
		payload: request
	}
}



export function updateUC(uc, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/ucs/${uc}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_UC,
		payload: request
	}
}




export function deleteUC(uc) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/ucs/${uc}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_UC,
		payload: request
	}
}