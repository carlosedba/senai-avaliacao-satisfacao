import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function searchSatisfacoes(params) {
	let search = `?q=${params.query}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/satisfacao/search${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.SEARCH_SATISFACOES,
		payload: request
	}
}




export function fetchSatisfacoes(dtuc, aluno) {
	let search = `?`

	if (dtuc) { search += `dtuc=${dtuc}` }
	if (aluno) { search += `aluno=${aluno}` }

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/satisfacao${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_SATISFACOES,
		payload: request
	}
}




export function fetchSatisfacoesPorDTUC(dtuc) {
	let search = `?dtuc=${dtuc}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/satisfacao/${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_SATISFACOES,
		payload: request
	}
}




export function fetchSatisfacao(satisfacao) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/satisfacao/${satisfacao}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_SATISFACAO,
		payload: request
	}
}





export function createSatisfacao(props) {
	props.resultado = JSON.stringify(props.resultado)

	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/satisfacao`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_SATISFACAO,
		payload: request
	}
}



export function updateSatisfacao(satisfacao, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/satisfacao/${satisfacao}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_SATISFACAO,
		payload: request
	}
}




export function deleteSatisfacao(satisfacao) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/satisfacao/${satisfacao}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_SATISFACAO,
		payload: request
	}
}




export function deleteSatisfacaoByDTUC(dtuc) {
	let search = `?dtuc=${dtuc}`

	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/satisfacao/delete${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_SATISFACAO,
		payload: request
	}
}