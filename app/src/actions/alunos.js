import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function searchAlunos(params) {
	let search = `?q=${params.query}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos/search${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.SEARCH_ALUNOS,
		payload: request
	}
}




export function fetchAlunos() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_ALUNOS,
		payload: request
	}
}




export function fetchAluno(aluno) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos/${aluno}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_ALUNO,
		payload: request
	}
}




export function fetchAlunoNome(nome) {
	let query = `?q=${nome}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos/nome/${query}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_ALUNO,
		payload: request
	}
}





export function createAluno(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/alunos`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_ALUNO,
		payload: request
	}
}



export function updateAluno(aluno, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/alunos/${aluno}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_ALUNO,
		payload: request
	}
}




export function deleteAluno(aluno) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/alunos/${aluno}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_ALUNO,
		payload: request
	}
}