import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchDocentes() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/docentes`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_DOCENTES,
		payload: request
	}
}




export function fetchDocente(professor) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/docentes/${professor}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_DOCENTE,
		payload: request
	}
}





export function createDocente(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/docentes`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_DOCENTE,
		payload: request
	}
}



export function updateDocente(professor, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/docentes/${professor}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_DOCENTE,
		payload: request
	}
}




export function deleteDocente(professor) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/docentes/${professor}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_DOCENTE,
		payload: request
	}
}