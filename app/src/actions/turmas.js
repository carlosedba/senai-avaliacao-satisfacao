import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchTurmas() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/turmas`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_TURMAS,
		payload: request
	}
}




export function fetchTurma(turma) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/turmas/${turma}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_TURMA,
		payload: request
	}
}





export function createTurma(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/turmas`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_TURMA,
		payload: request
	}
}



export function updateTurma(turma, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/turmas/${turma}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_TURMA,
		payload: request
	}
}




export function deleteTurma(turma) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/turmas/${turma}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_TURMA,
		payload: request
	}
}