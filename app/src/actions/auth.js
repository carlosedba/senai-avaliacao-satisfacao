import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'


export function fetchToken(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/token`,
		headers: []
	})

	return {
		type: types.AUTH_FETCH_TOKEN,
		payload: request
	}
}

