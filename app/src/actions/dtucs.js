import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function fetchDTUCs() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/docentes/turmas`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_DTUCS,
		payload: request
	}
}




export function fetchDTUCsByTurma(turma) {
	let search = `?turma=${turma}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/docentes/turmas/${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_DTUCS_TURMA,
		payload: request
	}
}




export function fetchDTUC(dtuc) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/docentes/turmas/${dtuc}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_DTUC,
		payload: request
	}
}





export function createDTUC(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/docentes/turmas`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_DTUC,
		payload: request
	}
}



export function updateDTUC(uc, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/docentes/turmas/${dtuc}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_DTUC,
		payload: request
	}
}




export function deleteDTUC(dtuc) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/docentes/turmas/${dtuc}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_DTUC,
		payload: request
	}
}