import axios from 'axios'
import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'
import * as types from '../constants/ActionTypes'

export function searchAlunosTurma(params) {
	let search = `?q=${params.query}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos/turmas/search${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.SEARCH_ALUNOS_TURMA,
		payload: request
	}
}




export function fetchAlunosTurma() {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos/turmas`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_ALUNOS_TURMA,
		payload: request
	}
}




export function fetchAlunosTurmaByTurma(turma) {
	let search = `?turma=${turma}`

	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos/turmas/${search}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_ALUNOS_TURMA,
		payload: request
	}
}




export function fetchAlunoTurma(aluno) {
	const request = axios({
		method: 'get',
		url: `${API_ENDPOINT}/alunos/turmas/${aluno}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.FETCH_ALUNO_TURMA,
		payload: request
	}
}





export function createAlunoTurma(props) {
	const request = axios({
		method: 'post',
		data: props,
		url: `${API_ENDPOINT}/alunos/turmas`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.CREATE_ALUNO_TURMA,
		payload: request
	}
}



export function updateAlunoTurma(aluno, props) {
	const request = axios({
		method: 'put',
		data: props,
		url: `${API_ENDPOINT}/alunos/turmas/${aluno}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.UPDATE_ALUNO_TURMA,
		payload: request
	}
}




export function deleteAlunoTurma(aluno) {
	const request = axios({
		method: 'delete',
		url: `${API_ENDPOINT}/alunos/turmas/${aluno}`,
		headers: {
			'X-Access-Token': store.get('token')
		},
	})

	return {
		type: types.DELETE_ALUNO_TURMA,
		payload: request
	}
}