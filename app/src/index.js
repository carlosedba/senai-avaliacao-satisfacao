import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, Link, useRouterHistory, hashHistory, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import { createHashHistory } from 'history'
import { DEV_BUILD } from './constants/Globals'

import setStore from './store'

import './libs/normalize/normalize.css'

import './libs/font-awesome/css/font-awesome.min.css'

import './libs/store/store.min.js'

import './libs/jquery/jquery-3.1.1.min.js'

import './libs/jsgrid/jsgrid.min.css'
import './libs/jsgrid/jsgrid.js'

import './libs/moment/moment-with-locales.js'

import './libs/chart/Chart.min.js'

import './libs/socket.io/socket.io.min.js'

import './libs/socket.io-stream/socket.io-stream.js'

import './libs/delivery/delivery.js'

import './assets/scss/main.scss'

import './assets/css/jsgrid-theme-senai.css'


//import App from './components/AppHRM'
import App from './components/App'
import Header from './components/Header'
import Index from './pages/Index'
import Login from './pages/Login'
import Logout from './pages/Logout'
import AvaliacaoSatisfacao from './pages/AvaliacaoSatisfacao'
import Painel from './pages/Painel'
import PainelAlunos from './pages/PainelAlunos'
import PainelDocentes from './pages/PainelDocentes'
import PainelCursos from './pages/PainelCursos'
import PainelSatisfacao from './pages/PainelSatisfacao'
import PainelTurmas from './pages/PainelTurmas'
import PainelUCs from './pages/PainelUCs'
import PainelUsuarios from './pages/PainelUsuarios'
import PainelNovoAluno from './pages/PainelNovoAluno'
import PainelNovoDocente from './pages/PainelNovoDocente'
import PainelNovoCurso from './pages/PainelNovoCurso'
import PainelNovaTurma from './pages/PainelNovaTurma'
import PainelNovaUC from './pages/PainelNovaUC'
import PainelImportarAlunos from './pages/PainelImportarAlunos'
import PainelImportarTurma from './pages/PainelImportarTurma'
import PainelNovoUsuario from './pages/PainelNovoUsuario'
import PainelEditarAluno from './pages/PainelEditarAluno'
import PainelEditarTurma from './pages/PainelEditarTurma'
import PainelEditarUC from './pages/PainelEditarUC'
import PainelEditarUsuario from './pages/PainelEditarUsuario'
import PainelEditarCurso from './pages/PainelEditarCurso'
import PainelEditarDocente from './pages/PainelEditarDocente'
import PainelVerSatisfacao from './pages/PainelVerSatisfacao'
import PainelImprimirGraficosSatisfacao from './pages/PainelImprimirGraficosSatisfacao'
import PainelImprimirObservacoesSatisfacao from './pages/PainelImprimirObservacoesSatisfacao'
import PainelObservacaoAvaliacao from './pages/PainelObservacaoAvaliacao'
//import x from './pages/x'

const history = useRouterHistory(createHashHistory)({
	basename: '/'
})

const store = setStore()

moment.locale('pt-br')

render((
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route path="/" component={App}>	
				<IndexRoute component={Index} />
				<Route path="avaliacao-satisfacao/:id" component={AvaliacaoSatisfacao} />

				<Route path="login" component={Login} />
				<Route path="logout" component={Logout} />
				<Route path="painel" component={Painel} />

				<Route path="painel/satisfacao" component={PainelSatisfacao} />
				<Route path="painel/satisfacao/:id" component={PainelVerSatisfacao} />
				<Route path="painel/satisfacao/:id/i/graficos" component={PainelImprimirGraficosSatisfacao} />
				<Route path="painel/satisfacao/:id/i/observacoes" component={PainelImprimirObservacoesSatisfacao} />
				<Route path="painel/satisfacao/:id/:aluno" component={PainelObservacaoAvaliacao} />

				<Route path="painel/alunos" component={PainelAlunos} />
				<Route path="painel/alunos/novo" component={PainelNovoAluno} />
				<Route path="painel/alunos/importar" component={PainelImportarAlunos} />
				<Route path="painel/alunos/editar/:id" component={PainelEditarAluno} />

				<Route path="painel/docentes" component={PainelDocentes} />
				<Route path="painel/docentes/novo" component={PainelNovoDocente} />
				<Route path="painel/docentes/editar/:id" component={PainelEditarDocente} />

				<Route path="painel/cursos" component={PainelCursos} />
				<Route path="painel/cursos/novo" component={PainelNovoCurso} />
				<Route path="painel/cursos/editar/:id" component={PainelEditarCurso} />

				<Route path="painel/turmas" component={PainelTurmas} />
				<Route path="painel/turmas/novo" component={PainelNovaTurma} />
				<Route path="painel/turmas/novo/importar" component={PainelImportarTurma} />
				<Route path="painel/turmas/editar/:id" component={PainelEditarTurma} />

				<Route path="painel/ucs" component={PainelUCs} />
				<Route path="painel/ucs/novo" component={PainelNovaUC} />
				<Route path="painel/ucs/editar/:id" component={PainelEditarUC} />

				<Route path="painel/usuarios" component={PainelUsuarios} />
				<Route path="painel/usuarios/novo" component={PainelNovoUsuario} />
				<Route path="painel/usuarios/editar/:id" component={PainelEditarUsuario} />
			</Route>
		</Router>
	</Provider>
), document.getElementById('root'))

