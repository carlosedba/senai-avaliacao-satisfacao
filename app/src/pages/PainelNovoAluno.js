import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import { createAluno } from '../actions/alunos'

import Loading from '../components/Loading'

@connect((store) => {
	return {}
})
export default class PainelNovoAluno extends Component {
	constructor(props) {
		super(props)

		this.id = getRandomInt(1000, 9999)

		this.state = {
			loading: false,

			aluno: {
				id: this.id,
				matricula: '',
				nome: '',
				cpf: '',
				nascimento: '',
				situacao: ''
			}
		}

		this.handleNomeChange 			= this.handleNomeChange.bind(this)
		this.handleMatriculaChange 		= this.handleMatriculaChange.bind(this)
		this.handleCPFChange 			= this.handleCPFChange.bind(this)
		this.handleNascimentoChange		= this.handleNascimentoChange.bind(this)
		this.handleSituacaoChange 		= this.handleSituacaoChange.bind(this)
		this.handleSubmit 				= this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}
	
	handleNomeChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { aluno: { nome: { $set: value } } }))
	}

	handleMatriculaChange(event) {
		let value = event.target.data
		this.setState(update(this.state, { aluno: { matricula: { $set: value } } }))
	}

	handleCPFChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { aluno: { cpf: { $set: value } } }))
	}

	handleNascimentoChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { aluno: { nascimento: { $set: value } } }))
	}

	handleSituacaoChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { aluno: { situacao: { $set: value } } }))
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })

		this.props.dispatch(createAluno(this.state.aluno))
			.then((res) => {
				if (res.action.type === 'CREATE_ALUNO_SUCCESS') {
					this.closeModal()
				} else if (res.action.type === 'CREATE_ALUNO_ERROR') {
					this.setState({ loading: false })
					alert('Não foi possível realizar o cadastro.')
				}
			})
	}

	closeModal() {
		return location.href = this.props.returnTo
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Novo Aluno</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --big">
									<label>Nome</label>
									<input type="text" value={this.state.aluno.nome} onChange={this.handleNomeChange} required/>
								</div>
								<div className="input --midmid">
									<label>Matrícula</label>
									<input type="text" value={this.state.aluno.matricula} onChange={this.handleMatriculaChange}/>
								</div>
								<div className="input --midmid">
									<label>CPF</label>
									<input type="text" value={this.state.aluno.cpf} onChange={this.handleCPFChange}/>
								</div>
								<div className="input --midmid">
									<label>Data de Nascimento</label>
									<input type="date" value={this.state.aluno.nascimento} onChange={this.handleNascimentoChange}/>
								</div>
								<div className="input --midmid">
									<label>Situação</label>
									<input type="text" value={this.state.aluno.situacao} onChange={this.handleSituacaoChange}/>
								</div>
								<button className="btn btn-alpha" type="submit">Cadastrar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




