import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import { createUsuario } from '../actions/usuarios'

import Loading from '../components/Loading'

@connect((store) => {
	return {}
})
export default class PainelNovoUsuario extends Component {
	constructor(props) {
		super(props)

		this.id = getRandomInt(1000, 9999)

		this.state = {
			loading: false,

			usuario: {
				id: this.id,
				nome: '',
				email: '',
				senha: '',
				confirmar_senha: '',
			}
		}

		this.handleNomeChange 				= this.handleNomeChange.bind(this)
		this.handleEmailChange 				= this.handleEmailChange.bind(this)
		this.handleSenhaChange 				= this.handleSenhaChange.bind(this)
		this.handleConfirmarSenhaChange 	= this.handleConfirmarSenhaChange.bind(this)
		this.handleSubmit					= this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	handleNomeChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { usuario: { nome: { $set: value } } }))
	}

	handleEmailChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { usuario: { email: { $set: value } } }))
	}

	handleSenhaChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { usuario: { senha: { $set: value } } }))
	}

	handleConfirmarSenhaChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { usuario: { confirmar_senha: { $set: value } } }))
	}
	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })

		if (this.state.usuario.senha !== this.state.usuario.confirmar_senha) {
			this.setState({ loading: false })
			alert("As senhas não batem.")
		} else {
			this.props.dispatch(createUsuario(this.state.usuario))
				.then((res) => {
					if (res.action.type === 'CREATE_USUARIO_SUCCESS') {
						this.closeModal()
					} else if (res.action.type === 'CREATE_USUARIO_ERROR') {
						this.setState({ loading: false })
						alert('Não foi possível realizar o cadastro.')
					}
				})
			}
	}

	closeModal() {
		return location.href = this.props.returnTo
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Novo Usuário</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --big">
									<label>Nome</label>
									<input type="text" value={this.state.usuario.nome} onChange={this.handleNomeChange} required/>
								</div>
								<div className="input --big">
									<label>E-mail</label>
									<input type="email" value={this.state.usuario.email} onChange={this.handleEmailChange} required/>
								</div>
								<div className="input --medium">
									<label>Nova Senha</label>
									<input type="password" value={this.state.usuario.senha} onChange={this.handleSenhaChange} required/>
								</div>
								<div className="input --medium">
									<label>Confirmar Senha</label>
									<input type="password" value={this.state.usuario.confirmar_senha} onChange={this.handleConfirmarSenhaChange} required/>
								</div>
								<button className="btn btn-alpha" type="submit">Cadastrar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




