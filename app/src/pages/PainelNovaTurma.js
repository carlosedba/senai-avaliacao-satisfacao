import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import async from 'async'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import { createTurma } from '../actions/turmas'
import { fetchCursos } from '../actions/cursos'
import { fetchDocentes } from '../actions/docentes'
import { fetchUCs } from '../actions/ucs'
import { createDTUC } from '../actions/dtucs'
import { createAlunoTurma } from '../actions/alunosTurma'
import { searchAlunos, fetchAlunoNome } from '../actions/alunos'

import Loading from '../components/Loading'

@connect((store) => {
	return {
		cursos: 		store.cursos.list,
		ucs: 			store.ucs.list,
		docentes: 		store.docentes.list,
		search: 		store.alunos.search.items,
	}
})
export default class PainelNovaTurma extends Component {
	constructor(props) {
		super(props)

		this.id = getRandomInt(1000, 9999)

		this.state = {
			loading: false,

			data: {
				turma: {
					id: this.id,
					nome: '',
					id_curso: '',
				},
				cursos: 		[],
				ucs: 			[],
				dtucs: 			[],
				docentes: 		[],
				modulos: 		[1, 2, 3, 4, 5, 6],
				alunos: 		[],
			},

			ui: {
				ucs: 			[],
				dtucs: 			[],
				modulos: 		[],
				alunos: 		[],
			},

			search: '',
			searchResults: [],
			focusedResultIndex: -1,
		}

		this.handleNomeChange	 			= this.handleNomeChange.bind(this)
		this.handleIdCursoChange 			= this.handleIdCursoChange.bind(this)
		this.handleDataInicioChange 		= this.handleDataInicioChange.bind(this)
		this.handleDataConclusaoChange 		= this.handleDataConclusaoChange.bind(this)
		this.handleSubmit 					= this.handleSubmit.bind(this)
		this.addDTUC 						= this.addDTUC.bind(this)
		this.removeDTUC 					= this.removeDTUC.bind(this)
		this.adicionarAlunos 				= this.adicionarAlunos.bind(this)

		this.handleSearchChange 			= this.handleSearchChange.bind(this)
		this.displayResults 				= this.displayResults.bind(this)
		this.moveCursorToEnd 				= this.moveCursorToEnd.bind(this)
		this.goToResult 					= this.goToResult.bind(this)

		this.renderDTUCS 					= this.renderDTUCS.bind(this)
		this.renderAlunos 					= this.renderAlunos.bind(this)
		this.removeAlunoTurma 				= this.removeAlunoTurma.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentWillUpdate(nextProps, nextState) {
		if (nextProps.location.state) {
			if (nextProps.location.state.import) {
				let data = nextProps.location.state.data
				this.adicionarAlunos(data)
				nextProps.location.state.import = false
			}
		}
	}

	componentDidMount() {
		this.props.dispatch(fetchCursos())
			.then((res) => {
				this.setState(update(this.state, { data: { cursos: { $set: res.action.payload.data } } }))
			})

		this.props.dispatch(fetchUCs())
			.then((res) => {
				this.setState(update(this.state, { data: { ucs: { $set: res.action.payload.data } } }))
			})

		this.props.dispatch(fetchDocentes())
			.then((res) => {
				this.setState(update(this.state, { data: { docentes: { $set: res.action.payload.data } } }))
			})

		this.component.addEventListener('keydown', this.handleKeyDown.bind(this))
	}

	notContainedIn(arr) {
		return function arrNotContains(element) {
			return arr.indexOf(element) === -1
		}
	}

	handleNomeChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { data: { turma: { nome: { $set: value } } } }))
	}

	handleIdCursoChange(event) {
		let value = event.target.value

		this.setState(update(this.state, { data: { turma: { id_curso: { $set: value } } } }))
		
		setTimeout(() => {
			this.updateUCsState(value)
			this.updateModulosState(value)
		})
	}

	handleDataInicioChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { data: { turma: { data_inicio: { $set: value } } } }))
	}

	handleDataConclusaoChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { data: { turma: { data_conclusao: { $set: value } } } }))
	}

	createAllDTUCs() {
		return new Promise((resolve, reject) => {
			async.map(this.state.data.dtucs, (el, callback) => {
				this.props.dispatch(createDTUC(el))
					.then((res) => {
						callback(null, res)
					})
			}, (err, results) => {
				resolve(results)
			})
		})
	}

	createAllAlunoTurma() {
		return new Promise((resolve, reject) => {
			async.map(this.state.data.alunos, (el, callback) => {
				this.props.dispatch(createAlunoTurma(el))
					.then((res) => {
						callback(null, res)
					})
			}, (err, results) => {
				resolve(results)
			})
		})
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })

		this.props.dispatch(createTurma(this.state.data.turma))
			.then((res) => {
				if (res.action.type === 'CREATE_TURMA_SUCCESS') {
					Promise.all([
						this.createAllDTUCs(),
						this.createAllAlunoTurma(),
					])
					.then((results) => {
						this.closePage()
					})
				} else if (res.action.type === 'CREATE_TURMA_ERROR') {
					this.setState({ loading: false })
					return alert('Não foi possível realizar o cadastro.')
				}
			})
	}

	updateUCsState(id) {
		this.state.data.cursos.map((curso, i) => {
			if (curso.id === id) {
				this.setState(update(this.state, {
					ui: { ucs: { $set: this.state.data.ucs } }
				}))

				this.state.data.ucs.forEach((el, ind, arr) => {
					if (el.id_curso !== curso.id) {
						this.setState(update(this.state, {
							ui: {
								ucs: { $set: this.state.data.ucs.filter((el, i) => {
									return el.id_curso === curso.id
								})}
							}
						}))
					}
				}, this)
			}
		})
	}

	updateModulosState(id) {
		this.state.data.cursos.map((curso, i) => {
			if (curso.id === id) {
				this.setState(update(this.state, {
					ui: { modulos: { $set: this.state.data.modulos } }
				}))

				this.setState(update(this.state, {
					ui: {
						modulos: { $set: this.state.data.modulos.filter((el, i) => {
							return el <= curso.modulos
						})}
					}
				}))
			}
		})
	}

	addDTUC(event) {
		let dtuc_id_uc 			= this.refs.dtuc_id_uc
		let dtuc_id_docente 	= this.refs.dtuc_id_docente
		let dtuc_modulo 		= this.refs.dtuc_modulo

		if (dtuc_id_uc.selectedIndex > 0 && dtuc_id_docente.selectedIndex > 0 && dtuc_modulo.selectedIndex > 0) {
			setTimeout(() => {
				this.setState(update(this.state, {
					data: {
						dtucs: { $push: [{
							id: 				getRandomInt(1000, 9999),
							id_docente: 		dtuc_id_docente.value,
							id_turma: 			this.id,
							id_uc: 				dtuc_id_uc.value,
							modulo: 			dtuc_modulo.value
						}]}
					}
				}))

				this.setState(update(this.state, {
					ui: {
						dtucs: { $push: [{
							uc: 				dtuc_id_uc.options[dtuc_id_uc.selectedIndex].innerText,
							docente: 			dtuc_id_docente.options[dtuc_id_docente.selectedIndex].innerText,
							modulo: 			dtuc_modulo.value
						}]}
					}
				}))
			})
		}
	}

	removeDTUC(event) {
		let index = event.target.dataset.index
		
		setTimeout(() => {
			this.setState(update(this.state, {
				data: {
					dtucs: { $set: this.state.data.dtucs.filter((el, i) => { return i != index }) }
				}
			}))
			
			this.setState(update(this.state, {
				ui: {
					dtucs: { $set: this.state.ui.dtucs.filter((el, i) => { return i != index }) }
				}
			}))
		})
	}

	addAlunoTurma(data, event) {
		let id_aluno = data.id
		let nome = data.nome

		if (id_aluno && nome !== '') {
			/*console.log({
				id: 				getRandomInt(1000, 9999),
				id_turma: 			this.id,
				id_aluno: 			id_aluno,
			})*/

			setTimeout(() => {
				this.setState(update(this.state, {
					data: {
						alunos: { $push: [{
							id: 				getRandomInt(1000, 9999),
							id_turma: 			this.id,
							id_aluno: 			id_aluno,
						}]}
					}
				}))

				this.setState(update(this.state, {
					ui: {
						alunos: { $push: [{
							nome: 				nome,
						}]}
					}
				}))

				this.setState({ search: '' })
				this.setState({ searchResults: [] })
			})
		}
	}

	removeAlunoTurma(event) {
		let index = event.target.dataset.index
		
		setTimeout(() => {
			this.setState(update(this.state, {
				data: {
					alunos: { $set: this.state.data.alunos.filter((el, i) => { return i != index }) }
				}
			}))
			
			this.setState(update(this.state, {
				ui: {
					alunos: { $set: this.state.ui.alunos.filter((el, i) => { return i != index }) }
				}
			}))
		})
	}

	moveCursorToEnd(e) {
		let value = e.target.value

		e.target.value = ''
		e.target.value = value
	}

	handleSearchChange(event) {
		let value = event.target.value

		this.setState({ search: value })
		this.setState({ focusedResultIndex: -1 })

		this.search(value)
	}

	handleKeyDown(event) {
		switch (event.keyCode) {
			case 13:
				this.goToResult()
				break
			case 38:
				if (this.state.searchResults.length > 0) {
					if (this.state.focusedResultIndex > -1) {
						this.setState({ focusedResultIndex: this.state.focusedResultIndex - 1 })
					}

					if (this.state.focusedResultIndex >= 0) {						
						let results = document.querySelectorAll('.alunos li')
						let focusedResult = results[this.state.focusedResultIndex]

						focusedResult.focus()
						this.input.dataset.userInput = this.input.value
						this.input.dataset.id = focusedResult.dataset.id
						this.input.dataset.nome = focusedResult.dataset.nome
						this.input.value = focusedResult.innerText
					}

					if (this.state.focusedResultIndex === -1) {
						this.input.focus()
						this.input.dataset.id = ''
						this.input.dataset.nome = ''
						this.input.value = this.input.dataset.userInput
					}
				}
				break
			case 40:
				if (this.state.searchResults.length > 0) {
					if (this.state.focusedResultIndex < this.state.searchResults.length - 1) {
						this.setState({ focusedResultIndex: this.state.focusedResultIndex + 1 })
					}

					if (this.state.focusedResultIndex < this.state.searchResults.length) {						
						let results = document.querySelectorAll('.alunos li')
						let focusedResult = results[this.state.focusedResultIndex]

						focusedResult.focus()
						this.input.dataset.userInput = this.input.value
						this.input.dataset.id = focusedResult.dataset.id
						this.input.dataset.nome = focusedResult.dataset.nome
						this.input.value = focusedResult.innerText
					}
				}
				break
		}
	}

	search(query) {
		if (query.length > 0) {
			this.props.dispatch(searchAlunos({
				query: query
			}))
			.then((res) => {
				let data = res.action.payload.data
				this.setState({ searchResults: data })
			})
		} else {
			setTimeout(() => { this.setState({ searchResults: [] }) })
		}
	}

	displayResults(result) {
		let id = result.id
		let nome = result.nome.toLowerCase()
		let busca = this.state.search.toLowerCase()

		let fim = nome.split(busca)[1]
		let inicio = nome.substring(0, nome.indexOf(fim))
		let hidden = ''

		if (nome.indexOf(fim) <= 0) {
			hidden = 'hidden'
		}

		return (
			<li key={id} tabIndex="0" className={hidden} data-id={id} data-nome={result.nome} onClick={this.addAlunoTurma.bind(this, { id: id, nome: result.nome })}>
				<span className="result">
					{inicio}
					<span className="bold-text">{fim}</span>
				</span>
			</li>
		)
	}

	goToResult(event) {
		if (this.state.searchResults.length > 0) {
			if (this.state.focusedResultIndex !== -1) {
				let results = document.querySelectorAll('.nova-turma-aluno-resultados li')
				let focusedResult = results[this.state.focusedResultIndex]
				let id = focusedResult.dataset.id
				let nome = focusedResult.dataset.nome

				this.addAlunoTurma({ id: id, nome: nome })
			}
		}
	}

	renderDTUCS() {
		return this.state.ui.dtucs.map((el, i) => {
			return (
				<div key={i} className="added-item">
					<div className="input --big">
						<span key={i} className="input-preview">{el.uc}</span>
					</div>
					<div className="input --medium">
						<span key={i} className="input-preview">{el.docente}</span>
					</div>
					<div className="input --medium">
						<span key={i} className="input-preview">{el.modulo}</span>
					</div>
					<button key={i} data-index={i} className="btn btn-delete" type="button" onClick={this.removeDTUC}>&#10006;</button>
				</div>
			)
		})
	}

	renderAlunos() {
		return this.state.ui.alunos.map((el, i) => {
			return (
				<div key={i} className="added-item">
					<div className="input --monster">
						<span key={i} className="input-preview">{el.nome}</span>
					</div>
					<button key={i} data-index={i} className="btn btn-delete" type="button" onClick={this.removeAlunoTurma}>&#10006;</button>
				</div>
			)
		})
	}

	adicionarAlunos(alunos) {
		let falhas = []

		async.map(alunos, (aluno, callback) => {
			this.props.dispatch(fetchAlunoNome(aluno.nome))
				.then((res) => {
					let data = res.action.payload.data
					let error = data.error

					if (error) {
						falhas.push(aluno)
					}
						
					callback(null, data)
				})
		}, (err, results) => {
			if (falhas.length > 0) {
				let alunos = ''

				falhas.map((el, i) => {
					alunos += `\n${el.nome} `
				})

				alert(`Erro - Os seguintes alunos não foram encontrados no sistema: ${alunos}`)
			} else {
				results.map((aluno, i) => {
					this.addAlunoTurma({ id: aluno.id, nome: aluno.nome })
				})
			}
		})
	}

	closePage() {
		return browserHistory.push('/painel/turmas')
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<main className="pagina-painel-auto">
						<div className="panel-header">
							<div className="panel-header-left">
								<div className="panel-titles">
									<h1 className="panel-title">Nova Turma</h1>
								</div>
							</div>
							<div className="panel-header-right">
								<Link className="btn btn-alpha"
									to={{ pathname: '/painel/turmas/novo/importar',
										state: { modal: true, returnTo: this.props.location.pathname } }}>Importar Turma</Link>
							</div>
						</div>
						<section className="turma">
							<span className="section-title-alpha">Informações</span>
							<div className="section-inputs">
								<div className="input --medium">
									<label>Nome</label>
									<input type="text" ref="nome" value={this.state.data.turma.nome || ''} onChange={this.handleNomeChange} required/>
								</div>
								<div className="multi-input --row --center ">		
									<div className="input --big">
										<label>Curso</label>
										<select value={this.state.data.turma.id_curso || ''} onChange={this.handleIdCursoChange} required>
											<option disabled>Selecione uma opção</option>
											{this.state.data.cursos.map((el, i) => {
												return <option key={i} value={el.id}>{el.nome}</option>
											})}
										</select>
									</div>
									<div className="input --midmid">
										<label>Data de Início</label>
										<input type="date" value={this.state.data.turma.data_inicio || ''} onChange={this.handleDataInicioChange}/>
									</div>
									<div className="input --midmid">
										<label>Data de Conclusão</label>
										<input type="date" value={this.state.data.turma.data_conclusao || ''} onChange={this.handleDataConclusaoChange}/>
									</div>
								</div>
							</div>
							<span className="section-title-alpha">UCs e Docentes</span>
							<div className="section-inputs --with-list">
								<div className="multi-input --row --center">
									<div className="input --big">
										<label>Unidade Curricular</label>
										<select defaultValue="true" ref="dtuc_id_uc">
											<option disabled value>Selecione uma opção</option>
											{this.state.ui.ucs.map((el, i) => {
												return <option key={i} value={el.id} data-curso={el.id_curso}>{el.nome}</option>
											})}
										</select>
									</div>	
									<div className="input --medium">
										<label>Docente</label>
										<select defaultValue="true" ref="dtuc_id_docente">
											<option disabled value>Selecione uma opção</option>
											{this.state.data.docentes.map((el, i) => {
												return <option key={i} value={el.id}>{el.nome}</option>
											})}
										</select>
									</div>		
									<div className="input --medium">
										<label>Módulo</label>
										<select defaultValue="true" ref="dtuc_modulo">
											<option disabled value>Selecione uma opção</option>
											{this.state.ui.modulos.map((el, i) => {
												return <option key={i} value={el}>{el}</option>
											})}
										</select>
									</div>
									<button className="btn btn-add" type="button" onClick={this.addDTUC}>+</button>
								</div>
							</div>
							<div className="added-items-list">
								{this.renderDTUCS()}
							</div>
							<span className="section-title-alpha">Alunos</span>
							<div className="section-inputs --with-list" ref={(el) => { this.component = el }}>
								<div className="multi-input --row --center">								
									<div className="input --monster">
										<label>Nome do Aluno</label>
										<input type="text" ref={(input) => this.input = input} value={this.state.search} onChange={this.handleSearchChange} onFocus={this.moveCursorToEnd} />
									</div>
									<div className="alunos">
										<ul>
											{this.state.searchResults.map((el, i) => {
												return this.displayResults(el)
											})}
										</ul>
									</div>
									<button className="btn btn-add" type="button" onClick={this.goToResult}>+</button>
								</div>
							</div>
							<div className="added-items-list">
								{this.renderAlunos()}
							</div>
							<button className="btn btn-cadastrar btn-alpha" type="submit">Cadastrar</button>
						</section>
					</main>
				</form>
			</div>
		)
	}
}




