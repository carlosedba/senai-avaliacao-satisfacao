import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

import { fetchCursos, deleteCurso } from '../actions/cursos'

@connect((store) => {
	return {
		cursos: store.cursos.list.items,
	}
})
export default class PainelCursos extends Component {
	constructor(props) {
		super(props)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this.props.dispatch(fetchCursos())
			.then((res) => {
				let data = res.action.payload.data

				$("#jsGrid").jsGrid({
					width: "100%",
					height: "400px",
					visible: true,

					filtering: false,
					inserting: false,
					editing: false,
					sorting: true,
					paging: true,

					noDataContent: "Nada encontrado.",
					confirmDeleting: false,
					deleteConfirm: "Tem certeza que deseja deletar este item?",
					pageIndex: 1,
					pageSize: 5,
					pageButtonCount: 15,
					pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
					pageNextText: "Próxima",
					pagePrevText: "Anterior",
					pageFirstText: "&laquo;",
					pageLastText: "&raquo;",
					pageNavigatorNextText: "...",
					pageNavigatorPrevText: "...",
					invalidMessage: "Dados Inválidos!",
					loadIndication: true,
					loadIndicationDelay: 500,
					loadMessage: "",
					loadShading: true,

					data: data,

					fields: [
						{ name: "id", 			title: "ID", 				type: "text", 	width: 50 },
						{ name: "nome",			title: "Nome", 				type: "text", 	width: 200 },
						{ name: "modulos",		title: "Módulos", 			type: "number", width: 200 },
						{ type: "control", 		width: 60, itemTemplate: (value, item) => {
							var result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments)

							var editar = document.createElement('button')
							editar.addEventListener('click', this.handleEdit.bind(this, item))
							editar.innerHTML = 'Editar'
							editar.setAttribute('class', 'btn btn-grid')

							var excluir = document.createElement('button')
							excluir.addEventListener('click', this.handleDelete.bind(this, item))
							excluir.innerHTML = 'Excluir'
							excluir.setAttribute('class', 'btn btn-grid')

							var wrapper = document.createElement('div')
							wrapper.setAttribute('class', 'panel-grid-buttons')
							wrapper.appendChild(editar)
							wrapper.appendChild(excluir)

							return result.add(wrapper)
						}}
					]
				})
			})
	}

	handleEdit(item) {
		browserHistory.push({
			pathname: `/painel/cursos/editar/${item.id}`,
			state: { modal: true, returnTo: this.props.location.pathname }
		})
	}

	handleDelete(item) {
		if (confirm('Tem certeza que deseja deletar este item?')) {
			this.props.dispatch(deleteCurso(item.id))
				.then((res) => {
					if (res.action.type === 'DELETE_CURSO_SUCCESS') {
						let data = res.action.payload.data
						let error = data.error

						if (error) {
							alert(`Erro ${error.code} - ${error.message}`)
						} else {
							$("#jsGrid").jsGrid("deleteItem", item)
						}
					} else {
						alert(`Erro 5008 - Problema na comunicação com o servidor`)
					}
				})
		}
	}

	render() {
		return (
			<div>
				<main className="pagina-painel">
					<div className="panel-header">
						<div className="panel-header-left">
							<div className="panel-titles">
								<h1 className="panel-title --with-button">Cursos</h1>
							</div>
							<Link className="btn btn-alpha"
								to={{ pathname: '/painel/cursos/novo',
									state: { modal: true, returnTo: this.props.location.pathname } }}>Novo Curso</Link>
						</div>
						<div className="panel-header-right"></div>
					</div>
					<div className="panel-grid">
						<div id="jsGrid"></div>
					</div>
				</main>
			</div>
		)
	}
}




