import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import { fetchSatisfacao } from '../actions/satisfacao'

@connect((store) => {
	return {}
})
export default class PainelObservacaoAvaliacao extends Component {
	constructor(props) {
		super(props)

		this.id = this.props.params.aluno

		this.state = {
			avaliacao: null,
		}
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this._fetchSatisfacao(this.id)
			.then((avaliacao) => {
				avaliacao.resultado = JSON.parse(avaliacao.resultado)
				this.setState({ avaliacao: avaliacao })
				console.log(this.state.avaliacao)
			})	
	}

	_fetchSatisfacao(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchSatisfacao(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_SATISFACAO_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	closeModal() {
		return browserHistory.push(this.props.returnTo)
	}

	render() {
		return (
			<div>
				<div className="column nowrap">
					<div className="modal-header">
						<span className="modal-title">{(this.state.avaliacao) && this.state.avaliacao.aluno.nome}</span>
					</div>
					<div className="modal-content">
						<div className="modal-inputs">
							<div className="input input-observacao">
								<label>Observação</label>
								<textarea value={(this.state.avaliacao) ? this.state.avaliacao.resultado.observacoes : ''} readonly/>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}




