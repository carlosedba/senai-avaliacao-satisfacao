import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

import { fetchAlunos, deleteAluno } from '../actions/alunos'
import { fetchTurmas } from '../actions/turmas'

@connect((store) => {
	return {
		turmas: store.turmas.list.items,
		alunos: store.alunos.list.items,
	}
})
export default class PainelAlunos extends Component {
	constructor(props) {
		super(props)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this._fetchAlunos()
			.then((alunos) => {
				alunos.forEach((aluno, ind, arr) => {
					aluno.nascimento = moment.utc(aluno.nascimento).format("DD/MM/YYYY")
				})

				$("#jsGrid").jsGrid({
					width: "100%",
					height: "425px",
					visible: true,

					filtering: false,
					inserting: false,
					editing: false,
					sorting: true,
					paging: true,

					noDataContent: "Nada encontrado.",
					confirmDeleting: false,
					deleteConfirm: "Tem certeza que deseja deletar este item?",
					pageIndex: 1,
					pageSize: 7,
					pageButtonCount: 15,
					pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} &nbsp; de &nbsp; {pageCount}",
					pageNextText: "Próxima",
					pagePrevText: "Anterior",
					pageFirstText: "&laquo;",
					pageLastText: "&raquo;",
					pageNavigatorNextText: "...",
					pageNavigatorPrevText: "...",
					invalidMessage: "Dados Inválidos!",
					loadIndication: true,
					loadIndicationDelay: 500,
					loadMessage: "",
					loadShading: true,
					
					data: alunos,

					fields: [
						{ name: "id", 			title: "ID", 				type: "text", 	width: 50 },
						{ name: "turma", 		title: "Turma", 			type: "text" },
						{ name: "matricula", 	title: "Matricula", 		type: "text", 	width: 65 },
						{ name: "nome",			title: "Nome", 				type: "text", 	width: 200 },
						{ name: "cpf", 			title: "CPF", 				type: "text" },
						{ name: "nascimento", 	title: "Data de Nascimento",type: "text" },
						{ name: "situacao", 	title: "Situação", 			type: "text" },
						{ type: "control", 		width: 95, itemTemplate: (value, item) => {
								var result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments)

								var editar = document.createElement('button')
								editar.addEventListener('click', this.handleEdit.bind(this, item))
								editar.innerHTML = 'Editar'
								editar.setAttribute('class', 'btn btn-grid')

								var excluir = document.createElement('button')
								excluir.addEventListener('click', this.handleDelete.bind(this, item))
								excluir.innerHTML = 'Excluir'
								excluir.setAttribute('class', 'btn btn-grid')

								var wrapper = document.createElement('div')
								wrapper.setAttribute('class', 'panel-grid-buttons')
								wrapper.appendChild(editar)
								wrapper.appendChild(excluir)

								return result.add(wrapper)
							}
						}
					]
				})
			})
	}

	_fetchAlunos() {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchAlunos())
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data

				data.map((el, i) => {
					if (el.turmas.length) {
						if (el.turmas.length > 1) {
							el.turma = `${el.turmas[0].nome}+`
						} else {
							el.turma = `${el.turmas[0].nome}`
						}
					}
				})
				
				;(type === 'FETCH_ALUNOS_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	handleEdit(item) {
		browserHistory.push({
			pathname: `/painel/alunos/editar/${item.id}`,
			state: { modal: true, returnTo: this.props.location.pathname }
		})
	}

	handleDelete(item) {
		if (confirm('Tem certeza que deseja deletar este item?')) {
			this.props.dispatch(deleteAluno(item.id))
				.then((res) => {
					if (res.action.type === 'DELETE_ALUNO_SUCCESS') {
						let data = res.action.payload.data
						let error = data.error

						if (error) {
							alert(`Erro ${error.code} - ${error.message}`)
						} else {
							$("#jsGrid").jsGrid("deleteItem", item)
						}
					} else {
						alert(`Erro 5008 - Problema na comunicação com o servidor`)
					}
				})
		}
	}

	render() {
		return (
			<div>
				<main className="pagina-painel">
					<div className="panel-header">
						<div className="panel-header-left">
							<div className="panel-titles">
								<h1 className="panel-title --with-button">Alunos</h1>
							</div>
							<div className="panel-actions">
								<Link className="btn btn-alpha"
									to={{ pathname: '/painel/alunos/novo',
										state: { modal: true, returnTo: this.props.location.pathname } }}>Novo Aluno</Link>
							</div>
						</div>
						<div className="panel-header-right">
							<Link className="btn btn-alpha"
								to={{ pathname: '/painel/alunos/importar',
									state: { modal: true, returnTo: this.props.location.pathname } }}>Importar Alunos</Link>
						</div>
					</div>
					<div className="panel-grid">
						<div id="jsGrid"></div>
					</div>
				</main>
			</div>
		)
	}
}




