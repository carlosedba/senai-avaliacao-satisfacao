import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import Grafico from '../components/Grafico'

import { fetchDTUC } from '../actions/dtucs'
import { fetchSatisfacoesPorDTUC } from '../actions/satisfacao'
import { fetchAlunosTurmaByTurma } from '../actions/alunosTurma'

import senai_cinza from '../assets/img/senai_cinza.png'

@connect((store) => {
	return {
		avaliacoes: store.satisfacao.list,
	}
})
export default class PainelImprimirObservacoesSatisfacao extends Component {
	constructor(props) {
		super(props)

		this.id = this.props.params.id

		this.state = {
			dtuc: 					null,
			avaliacoes: 			[],
			alunos: 				[],
			alunosAvaliacao: 		{},
		}

		this.handleGoBack = this.handleGoBack.bind(this)
		this.handlePrint = this.handlePrint.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this._fetchDTUC(this.id)
			.then((dtuc) => {
				this.setState({ dtuc: dtuc })

				this._fetchAlunosTurmaByTurma(dtuc.id_turma)
					.then((data) => {
						this.setState({ alunos: data })
					})
			})

		this._fetchSatisfacoesPorDTUC(this.id)
			.then((avaliacoes) => {
				avaliacoes.map((el, i) => {
					el.resultado = JSON.parse(el.resultado)

					this.setState(update(this.state, { avaliacoes: { $push: [el] } }))
					this.setState(update(this.state, { alunosAvaliacao: { [el.id_aluno]: { $set: el }  } }))
				})
			})
			.then(() => {
				setTimeout(() => {
					window.print()
				}, 500)
			})
	}

	_fetchDTUC(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchDTUC(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_DTUC_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchSatisfacoesPorDTUC(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchSatisfacoesPorDTUC(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_SATISFACOES_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchAlunosTurmaByTurma(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchAlunosTurmaByTurma(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_ALUNOS_TURMA_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})

		})
	}

	handleAvaliacaoAluno(aluno) {
		let avaliacao = this.state.alunosAvaliacao[aluno]
		if (avaliacao) {
			browserHistory.push({
				pathname: `/painel/satisfacao/${this.id}/${avaliacao.id}`,
				state: { modal: true, returnTo: this.props.location.pathname }
			})
		}
	}

	handleGoBack(event) {
		return browserHistory.goBack()
	}

	handlePrint(event) {
		return window.print()
	}

	render() {
		return (
			<div>
				<main className="pagina-impressao">
					<nav className="panel-printbar">
						<div className="panel-printbar-left">
							<ul>
								<li><button className="btn btn-alpha" onClick={this.handleGoBack}>Voltar</button></li>
							</ul>
						</div>
						<div className="panel-printbar-right">
							<ul>
								<li><button className="btn btn-alpha" onClick={this.handlePrint}>Imprimir</button></li>
							</ul>
						</div>
					</nav>
					<div className="panel-header --with-line">
						<div className="panel-header-left">
							<div className="panel-titles">
								<h1 className="panel-title">{(this.state.dtuc) && `${this.state.dtuc.docente.nome}`}</h1>
								<h2 className="panel-subtitle">{(this.state.dtuc) && `${this.state.dtuc.turma.nome} - ${this.state.dtuc.uc.nome}`}</h2>
								<h3 className="panel-lowertitle">SENAI BOQUEIRÃO</h3>
							</div>
						</div>
						<div className="panel-header-right">
							<img className="logo-senai-imprimir" src={senai_cinza} />
						</div>
					</div>
					<section className="ver-satisfacao">
						<section className="column nowrap">
							<span className="section-title-alpha">Observações</span>
								<div className="satisfacao-observacoes">
									{this.state.avaliacoes.map((el, i) => {
										let observacao = (el.resultado.observacoes) ? el.resultado.observacoes : ''
										return (observacao !== '') && (
											<div key={i} className="satisfacao-observacao">
												<span className="satisfacao-observacao-nome">{el.aluno.nome}</span>
												<p className="satisfacao-observacao-texto">{observacao}</p>
											</div>
										)
									})}
								</div>
						</section>
					</section>
				</main>
			</div>
		)
	}
}




