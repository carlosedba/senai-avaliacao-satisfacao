import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { fetchOption } from '../actions/options'
import { fetchAluno } from '../actions/alunos'
import { fetchDTUCsByTurma } from '../actions/dtucs'
import { fetchUC } from '../actions/ucs'
import { createSatisfacao } from '../actions/satisfacao'

import { getRandomInt } from '../utils/math'

import senai_azul from '../assets/img/senai_azul.png'
import senai_cinza from '../assets/img/senai_cinza.png'
import satisfacao from '../assets/img/satisfacao.png'

@connect((store) => {
	return {}
})
export default class AvaliacaoSatisfacao extends Component {
	constructor(props) {
		super(props)

		this.id = getRandomInt(1000, 9999)
		this.aluno = this.props.params.id

		this.state = {
			step: 0,
			turmas: [],
			dtucs: [],
			ucs: [],
			avaliacao: {
				id: this.id,
				id_turma: '',
				id_aluno: this.aluno,
				id_dtuc: '',
				resultado: {
					objetivoEConteudo: {
						conteudos: '',
						conhecimentos: '',
					},
					docentesTecnicosDeEnsino: {
						relacionamento: '',
						dominio: '',
						capacidade: '',
						aulas: '',
						postura: '',
					},
					recursosDidaticos: {
						didatico: '',
						apoio: '',
					},
					infraestrutura: {
						sala: '',
						laboratorios: '',
						equipamentos: '',
					},
					observacoes: '',
				},
			},
			verificacao: {
				objetivoEConteudo: {
					conteudos: 0,
					conhecimentos: 0,
				},
				docentesTecnicosDeEnsino: {
					relacionamento: 0,
					dominio: 0,
					capacidade: 0,
					aulas: 0,
					postura: 0,
				},
				recursosDidaticos: {
					didatico: 0,
					apoio: 0,
				},
				infraestrutura: {
					sala: 0,
					laboratorios: 0,
					equipamentos: 0,
				},
			},
		},

		this.handleStep0Change			 = this.handleStep0Change.bind(this)
		this.handleStep0				 = this.handleStep0.bind(this)
		this.handleObservacoesChange	 = this.handleObservacoesChange.bind(this)
		this.handleSubmit				 = this.handleSubmit.bind(this)
	}

	componentWillMount() {
		this._fetchOption('index_satisfacao')
			.then((option) => {
				let value = JSON.parse(option.value)
				if (!value) browserHistory.push('/')
			})
	}

	componentDidMount() {		
		this._fetchAluno(this.aluno)
		.then((aluno) => {
			this.setState(update(this.state, { avaliacao: { id_turma: { $set: aluno.id_turma } } }))

			aluno.turmas.map((turma, i) => {
				this.setState(update(this.state, { turmas: { $push: [turma] } }))
				this.setState(update(this.state, { dtucs: { $push: [[]] } }))

				this._fetchDTUCsByTurma(turma.id)
				.then((dtucs) => {
					dtucs.map((dtuc) => {
						this.setState(update(this.state, { dtucs: { [i]: { $push: [dtuc] } } }))

						this._fetchUC(dtuc.id_uc)
						.then((uc) => {
							this.setState(update(this.state, { ucs: { $push: [uc] } }))
						})
					})
				})
			})
		})
	}

	_fetchOption(name) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchOption(name))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_OPTION_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchAluno(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchAluno(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_ALUNO_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchDTUCsByTurma(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchDTUCsByTurma(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_DTUCS_TURMA_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchUC(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchUC(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_UC_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	avalicaoRespondida() {
		return Object.keys(this.state.verificacao).every((area, i) => {
			return Object.keys(this.state.verificacao[area]).every((item, i) => {
				let verificacao = this.state.verificacao[area][item]
				if (verificacao === 0) {
					return false
				} else {
					return true
				}
			})
		})
	}

	handleStep0Change(event) {
		let value = event.target.value
		this.setState(update(this.state, { avaliacao: { id_dtuc: { $set: value } } }))
	}

	handleRadioChange(area, event) {
		let item = event.target.name
		let value = event.target.value
		this.setState(update(this.state, { avaliacao: { resultado: { [area]: { [item]: { $set: value } } } } }), () => {
			this.setState(update(this.state, { verificacao: { [area]: { [item]: { $set: 1 } } } }))
		})
		
	}

	handleStep0(event) {
		let value = event.target.value
		;(this.state.avaliacao.id_dtuc !== '') && this.setState({ step: 1 })
	}

	handleObservacoesChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { avaliacao: { resultado: { observacoes: { $set: value } } } }))
	}

	handleSubmit(event) {
		if (this.avalicaoRespondida()) {
			this.props.dispatch(createSatisfacao(this.state.avaliacao))
				.then((res) => {
					if (res.action.type === 'CREATE_SATISFACAO_SUCCESS') {
						this.setState({ step: 2 })
					} else if (res.action.type === 'CREATE_SATISFACAO_ERROR') {
						alert('Não foi possível realizar o cadastro.')
					}
				})
		} else {
			alert('Volte e responda todas as questões.')
		}
	}

	render() {
		return (
			<div>
				<main className={(this.state.step !== 1) ? 'pagina-avaliacao-satisfacao --step0' : 'pagina-avaliacao-satisfacao'}>
				{(this.state.step === 0) ?
					<div className="satisfacao">
						<section className="satisfacao-header">
							<img className="satisfacao-senai" src={senai_azul} />
							<img className="satisfacao-logo" src={satisfacao} />
							<span className="satisfacao-separator"></span>
							<div className="satisfacao-step0">
								<span className="satisfacao-area-title">Selecione uma Unidade Curricular</span>
								{this.state.dtucs.map((dtucs, i) => {
									return (dtucs.length) && (
										<div key={i} className="satisfacao-step0-curso" key={i}>
											<span className="input-title">{dtucs[0].turma.curso.nome}</span>
											<div className="inputs">
												{dtucs.map((dtuc, i) => {
													return (
														<div key={dtuc.id} className="input-radio">
															<input type="radio" name="step0" value={dtuc.id}
																	checked={this.state.avaliacao.id_dtuc === dtuc.id}
																	onChange={this.handleStep0Change}/>
															<span className="input-radio-title">{dtuc.uc.nome}</span>
														</div>
													)
												})}
											</div>
										</div>
									)
								})}
								<button className="btn btn-beta" onClick={this.handleStep0}>Continuar</button>
							</div>
						</section>
					</div>
				: '' }
				{(this.state.step === 1) ?
				<div className="satisfacao">
						<section className="satisfacao-header">
							<img className="satisfacao-senai" src={senai_azul} />
							<img className="satisfacao-logo" src={satisfacao} />
							<span className="satisfacao-separator"></span>
							<p className="satisfacao-descricao">Procurando levantar junto aos seus clientes a satisfação referente ao curso do qual você está participando, solicitamos preencher os dados abaixo e assinalar o grau que melhor represente a qualidade deste curso.</p>
						</section>
						<section className="satisfacao-area">
							<span className="satisfacao-area-title">Objetivo e Conteúdo</span>
							<div className="input">
								<span className="input-title">Conteúdos ministrados</span>
								<span className="input-description">Adequação dos conteúdos ministrados conforme o proposto no curso.</span>
								<div className="input-radio">
									<input type="radio" name="conteudos" value="5"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conteudos === '5'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conteudos" value="4"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conteudos === '4'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conteudos" value="3"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conteudos === '3'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conteudos" value="2"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conteudos === '2'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conteudos" value="1"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conteudos === '1'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conteudos" value="0"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conteudos === '0'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Conhecimentos aplicados na atividade profissional</span>
								<span className="input-description">Os conhecimentos estudados no curso são aplicáveis na atividade profissional do aluno.</span>
								<div className="input-radio">
									<input type="radio" name="conhecimentos" value="5"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conhecimentos === '5'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conhecimentos" value="4"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conhecimentos === '4'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conhecimentos" value="3"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conhecimentos === '3'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conhecimentos" value="2"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conhecimentos === '2'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conhecimentos" value="1"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conhecimentos === '1'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="conhecimentos" value="0"
									checked={this.state.avaliacao.resultado.objetivoEConteudo.conhecimentos === '0'}
									onChange={this.handleRadioChange.bind(this, 'objetivoEConteudo')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<span className="satisfacao-area-title">Docentes/Técnicos de Ensino</span>
							<div className="input">
								<span className="input-title">Relacionamento do docente/técnico com os alunos</span>
								<span className="input-description">Relacionamento do docente/técnico de ensino com os alunos, considerando o respeito, atenção, imparcialidade e com humor.</span>
								<div className="input-radio">
									<input type="radio" name="relacionamento" value="5"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.relacionamento === '5'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="relacionamento" value="4"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.relacionamento === '4'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="relacionamento" value="3"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.relacionamento === '3'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="relacionamento" value="2"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.relacionamento === '2'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="relacionamento" value="1"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.relacionamento === '1'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="relacionamento" value="0"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.relacionamento === '0'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Domínio de conteúdo do docente/técnico de ensino</span>
								<span className="input-description">Capacidade do docente/técnico de ensino em deselvolver o assunto com o domínio de conteúdo.</span>
								<div className="input-radio">
									<input type="radio" name="dominio" value="5"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.dominio === '5'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="dominio" value="4"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.dominio === '4'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="dominio" value="3"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.dominio === '3'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="dominio" value="2"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.dominio === '2'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="dominio" value="1"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.dominio === '1'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="dominio" value="0"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.dominio === '0'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Capaciadade de comunicação</span>
								<span className="input-description">Capacidade de comunicação do docente/técnico de ensino, em esclarecer dúvidas, promovendo a troca de experiências.</span>
								<div className="input-radio">
									<input type="radio" name="capacidade" value="5"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.capacidade === '5'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="capacidade" value="4"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.capacidade === '4'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="capacidade" value="3"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.capacidade === '3'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="capacidade" value="2"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.capacidade === '2'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="capacidade" value="1"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.capacidade === '1'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="capacidade" value="0"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.capacidade === '0'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Aulas Práticas</span>
								<span className="input-description">Demonstrações das aulas práticas planejadas e relacionadas ao tema, de forma a facilitar a assimilação do conhecimento.</span>
								<div className="input-radio">
									<input type="radio" name="aulas" value="5"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.aulas === '5'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="aulas" value="4"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.aulas === '4'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="aulas" value="3"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.aulas === '3'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="aulas" value="2"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.aulas === '2'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="aulas" value="1"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.aulas === '1'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="aulas" value="0"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.aulas === '0'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Postura do docente/técnico de ensino</span>
								<span className="input-description">Atitude dos docentes/técnicos de ensino quanto ao cumprimento de horário.</span>
								<div className="input-radio">
									<input type="radio" name="postura" value="5"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.postura === '5'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="postura" value="4"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.postura === '4'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="postura" value="3"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.postura === '3'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="postura" value="2"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.postura === '2'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="postura" value="1"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.postura === '1'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="postura" value="0"
									checked={this.state.avaliacao.resultado.docentesTecnicosDeEnsino.postura === '0'}
									onChange={this.handleRadioChange.bind(this, 'docentesTecnicosDeEnsino')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<span className="satisfacao-area-title">Recursos Didáticos</span>
							<div className="input">
								<span className="input-title">Material Didático</span>
								<span className="input-description">Apresentação com qualidade e fácil identificação dos conteúdos.</span>
								<div className="input-radio">
									<input type="radio" name="didatico" value="5"
									checked={this.state.avaliacao.resultado.recursosDidaticos.didatico === '5'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="didatico" value="4"
									checked={this.state.avaliacao.resultado.recursosDidaticos.didatico === '4'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="didatico" value="3"
									checked={this.state.avaliacao.resultado.recursosDidaticos.didatico === '3'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="didatico" value="2"
									checked={this.state.avaliacao.resultado.recursosDidaticos.didatico === '2'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="didatico" value="1"
									checked={this.state.avaliacao.resultado.recursosDidaticos.didatico === '1'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="didatico" value="0"
									checked={this.state.avaliacao.resultado.recursosDidaticos.didatico === '0'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Material de Apoio</span>
								<span className="input-description">Quadro (giz branco, magnético), projetor, vídeos e audiovisuais do SENAI, são adequados ao desenvolvimento das atividades.</span>
								<div className="input-radio">
									<input type="radio" name="apoio" value="5"
									checked={this.state.avaliacao.resultado.recursosDidaticos.apoio === '5'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="apoio" value="4"
									checked={this.state.avaliacao.resultado.recursosDidaticos.apoio === '4'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="apoio" value="3"
									checked={this.state.avaliacao.resultado.recursosDidaticos.apoio === '3'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="apoio" value="2"
									checked={this.state.avaliacao.resultado.recursosDidaticos.apoio === '2'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="apoio" value="1"
									checked={this.state.avaliacao.resultado.recursosDidaticos.apoio === '1'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="apoio" value="0"
									checked={this.state.avaliacao.resultado.recursosDidaticos.apoio === '0'}
									onChange={this.handleRadioChange.bind(this, 'recursosDidaticos')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<span className="satisfacao-area-title">Infraestrutura</span>
							<div className="input">
								<span className="input-title">Sala de Aula</span>
								<span className="input-description">Organizadas com iluminação e limpeza adequadas para o desenvolvimento da aula.</span>
								<div className="input-radio">
									<input type="radio" name="sala" value="5"
									checked={this.state.avaliacao.resultado.infraestrutura.sala === '5'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="sala" value="4"
									checked={this.state.avaliacao.resultado.infraestrutura.sala === '4'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="sala" value="3"
									checked={this.state.avaliacao.resultado.infraestrutura.sala === '3'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="sala" value="2"
									checked={this.state.avaliacao.resultado.infraestrutura.sala === '2'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="sala" value="1"
									checked={this.state.avaliacao.resultado.infraestrutura.sala === '1'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="sala" value="0"
									checked={this.state.avaliacao.resultado.infraestrutura.sala === '0'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Laboratórios e/ou Oficinas</span>
								<span className="input-description">Organizadas e em condições para o desenvolvimento das atividades práticas.</span>
								<div className="input-radio">
									<input type="radio" name="laboratorios" value="5"
									checked={this.state.avaliacao.resultado.infraestrutura.laboratorios === '5'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="laboratorios" value="4"
									checked={this.state.avaliacao.resultado.infraestrutura.laboratorios === '4'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="laboratorios" value="3"
									checked={this.state.avaliacao.resultado.infraestrutura.laboratorios === '3'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="laboratorios" value="2"
									checked={this.state.avaliacao.resultado.infraestrutura.laboratorios === '2'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="laboratorios" value="1"
									checked={this.state.avaliacao.resultado.infraestrutura.laboratorios === '1'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="laboratorios" value="0"
									checked={this.state.avaliacao.resultado.infraestrutura.laboratorios === '0'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Equipamentos e ferramentas</span>
								<span className="input-description">Adequadas, limpas, em condições de uso e disponíveis para o desenvolvimento da atividade prática.</span>
								<div className="input-radio">
									<input type="radio" name="equipamentos" value="5"
									checked={this.state.avaliacao.resultado.infraestrutura.equipamentos === '5'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Ótimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="equipamentos" value="4"
									checked={this.state.avaliacao.resultado.infraestrutura.equipamentos === '4'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Bom</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="equipamentos" value="3"
									checked={this.state.avaliacao.resultado.infraestrutura.equipamentos === '3'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Regular</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="equipamentos" value="2"
									checked={this.state.avaliacao.resultado.infraestrutura.equipamentos === '2'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Ruim</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="equipamentos" value="1"
									checked={this.state.avaliacao.resultado.infraestrutura.equipamentos === '1'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Péssimo</span>
								</div>
								<div className="input-radio">
									<input type="radio" name="equipamentos" value="0"
									checked={this.state.avaliacao.resultado.infraestrutura.equipamentos === '0'}
									onChange={this.handleRadioChange.bind(this, 'infraestrutura')}/>
									<span className="input-radio-title">Não se aplica</span>
								</div>
							</div>
							<div className="input">
								<span className="input-title">Observações</span>
								<span className="input-description">Utilize o campo abaixo para deixar suas observações, sugestões ou oportunidades de melhoria das aulas.</span>
								<textarea
									value={this.state.avaliacao.resultado.observacoes}
									onChange={this.handleObservacoesChange} />
							</div>

							<button className="btn btn-beta" onClick={this.handleSubmit}>Finalizar</button>
						</section>
						<section className="satisfacao-footer">
							<img className="satisfacao-senai" src={senai_cinza} />
						</section>
					</div>
				: ''}
				{(this.state.step === 2) ?
					<div className="satisfacao">
						<section className="satisfacao-header">
							<img className="satisfacao-senai" src={senai_azul} />
							<img className="satisfacao-logo" src={satisfacao} />
							<span className="satisfacao-separator"></span>
							<div className="satisfacao-step2">
								<span className="satisfacao-step2-message">Obrigado por realizar a pesquisa!</span>
								<Link className="btn btn-alpha" to="/">Ir para o Início</Link>
							</div>
						</section>
					</div>
				: ''}
				</main>
			</div>
		)
	}
}




