import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged, login } from '../utils/auth'

import sistema from '../assets/img/sistema.png'


@connect((store) => {
	return {}
})
export default class Login extends Component {
	constructor(props) {
		super(props)

		this.state = {
			erro: 0,
			login: {
				email: '',
				senha: '',
			}
		}

		this.handleEmailChange 			= this.handleEmailChange.bind(this)
		this.handleSenhaChange 			= this.handleSenhaChange.bind(this)
		this.handleSubmit 				= this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (isLogged()) browserHistory.push('/painel')
	}

	_login() {
		login(this.state.login, (err, token) => {
			if (err) {
				this.setState({ erro: err.code })
			} else {
				browserHistory.push('/painel')
			}
		})
	}

	handleEmailChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { login: { email: { $set: value } } }))
	}

	handleSenhaChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { login: { senha: { $set: value } } }))
	}

	handleSubmit(event) {
		event.preventDefault()
		this._login()
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<main className="pagina-login">
						<section className="login">
							<img className="login-logo-sistema" src={sistema} />
							<h1 className="login-title">Entrar</h1>
							<p className="login-description">Use seu e-mail e senha para logar.</p>
							<div className="inputs">
								<div className={(this.state.erro === 5003) ? 'input --error' : 'input'}>
									{(this.state.erro === 5003) &&
										<span className="input-error-message">Endereço de e-mail incorreto.</span>
									}
									<input type="email" placeholder="Endereço de E-mail" required value={this.state.email} onChange={this.handleEmailChange} />
								</div>
								<div className={(this.state.erro === 5002) ? 'input --error' : 'input'}>
									{(this.state.erro === 5002) &&
										<span className="input-error-message">Senha incorreta.</span>
									}
									<input type="password" placeholder="Senha" required value={this.state.senha} onChange={this.handleSenhaChange} />
								</div>
							</div>
							<button className="btn btn-login" type="submit">Entrar</button>
						</section>
					</main>
				</form>
			</div>
		)
	}
}




