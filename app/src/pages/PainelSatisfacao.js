import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

import { fetchOption, updateOption } from '../actions/options'
import { fetchDTUCs } from '../actions/dtucs'
import { fetchSatisfacoes, fetchSatisfacao, deleteSatisfacaoByDTUC } from '../actions/satisfacao'
import { fetchTurmas } from '../actions/turmas'

@connect((store) => {
	return {
		satisfacoes: store.satisfacao.list,
	}
})
export default class PainelSatisfacao extends Component {
	constructor(props) {
		super(props)

		this.state = {
			index_satisfacao: null,

			dtucs: [],
			avaliacoes: [],
		}

		this.handleIndexAvaliacao = this.handleIndexAvaliacao.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		let app = this

		this._fetchOption('index_satisfacao')
			.then((option) => {
				let value = JSON.parse(option.value)
				this.setState({ index_satisfacao: value })
			})

		this.avaliacoesPorDTUC((dtucs) => {
			$("#jsGrid").jsGrid({
				width: "100%",
				height: "400px",
				visible: true,

				filtering: false,
				inserting: false,
				editing: false,
				sorting: true,
				paging: true,

				noDataContent: "Nada encontrado.",
				confirmDeleting: false,
				deleteConfirm: "Tem certeza que deseja deletar este item?",
				pageIndex: 1,
				pageSize: 5,
				pageButtonCount: 15,
				pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
				pageNextText: "Próxima",
				pagePrevText: "Anterior",
				pageFirstText: "&laquo;",
				pageLastText: "&raquo;",
				pageNavigatorNextText: "...",
				pageNavigatorPrevText: "...",
				invalidMessage: "Dados Inválidos!",
				loadIndication: true,
				loadIndicationDelay: 500,
				loadMessage: "",
				loadShading: true,

				data: dtucs,

				rowClick: function(args) {
					if (!args.event.target.classList.contains('btn')) {
						app.handleOpen(args.item)
					}
				},

				fields: [
					{ name: "id", 					title: "ID", 				type: "text", 	width: 50 },
					{ name: "docente.nome", 		title: "Docente", 			type: "text", 	width: 200 },
					{ name: "turma.curso.nome", 	title: "Curso", 			type: "text", 	width: 175 },
					{ name: "turma.nome", 			title: "Turma", 			type: "text", 	width: 115 },
					{ name: "uc.nome",				title: "UC", 				type: "text", 	width: 175 },
					{ type: "control", 				width: 90, itemTemplate: (value, item) => {
						var result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments)

						var abrir = document.createElement('button')
						abrir.addEventListener('click', this.handleOpen.bind(this, item))
						abrir.innerHTML = 'Abrir'
						abrir.setAttribute('class', 'btn btn-grid')

						var excluir = document.createElement('button')
						excluir.addEventListener('click', this.handleDelete.bind(this, item))
						excluir.innerHTML = 'Excluir'
						excluir.setAttribute('class', 'btn btn-grid')

						var wrapper = document.createElement('div')
						wrapper.setAttribute('class', 'panel-grid-buttons')
						wrapper.appendChild(abrir)
						wrapper.appendChild(excluir)

						return result.add(wrapper)
					}}
				]
			})
		})
	}

	_fetchOption(name) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchOption(name))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_OPTION_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_updateOption(name, props) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(updateOption(name, props))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'UPDATE_OPTION_SUCCESS') ? resolve(data) : reject(`Error updating data!`)
			})
		})
	}

	_fetchDTUCs() {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchDTUCs())
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_DTUCS_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchSatisfacoes() {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchSatisfacoes())
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_SATISFACOES_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	avaliacoesPorDTUC(callback) {
		let dtucs = []

		this._fetchDTUCs()
		.then((data) => {
			data.map((el, i) => {
				this.setState(update(this.state, { dtucs: { [el.id]: { $set: el }  } }))
				this.setState(update(this.state, { avaliacoes: { [el.id]: { $set: [] }  } }))
			})

			this._fetchSatisfacoes()
			.then((data) => {
				data.map((el, i) => {
					this.setState(update(this.state, { avaliacoes: { [el.dtuc.id]: { $push: [el] }  } }))
				})

				for (let key in this.state.avaliacoes) {
					if (this.state.avaliacoes.hasOwnProperty(key)) {
						if (this.state.avaliacoes[key].length) {
							dtucs.push(this.state.dtucs[key])
						}
					}
				}

				callback(dtucs)
			})
		})
	}

	handleIndexAvaliacao(event) {
		if (this.state.index_satisfacao) {
			this._updateOption('index_satisfacao', { value: false })
				.then((res) => {
					this.setState({ index_satisfacao: false })
				})
		} else {
			this._updateOption('index_satisfacao', { value: true })
				.then((res) => {
					this.setState({ index_satisfacao: true })
				})
		}
	}

	handleOpen(item) {
		browserHistory.push({
			pathname: `/painel/satisfacao/${item.id}`,
		})
	}

	handleDelete(item) {
		if (confirm('Tem certeza que deseja deletar este item?')) {
			this.props.dispatch(deleteSatisfacaoByDTUC(item.id))
				.then((res) => {
					if (res.action.type === 'DELETE_SATISFACAO_SUCCESS') {
						let data = res.action.payload.data
						let error = data.error

						if (error) {
							alert(`Erro ${error.code} - ${error.message}`)
						} else {
							$("#jsGrid").jsGrid("deleteItem", item)
						}
					} else {
						alert(`Erro 5008 - Problema na comunicação com o servidor`)
					}
				})
		}
	}

	render() {
		return (
			<div>
				<main className="pagina-painel">
					<div className="panel-header">
						<div className="panel-header-left">
							<div className="panel-titles">
								<h1 className="panel-title --with-button">Avaliações de Satisfação</h1>
							</div>
							{(this.state.index_satisfacao !== null) && (
								<button className="btn btn-alpha" onClick={this.handleIndexAvaliacao}>{(this.state.index_satisfacao) ? 'Fechar Acesso' : 'Abrir Acesso'}</button>
							)}
						</div>
						<div className="panel-header-right"></div>
					</div>
					<div className="panel-grid">
						<div id="jsGrid"></div>
					</div>
				</main>
			</div>
		)
	}
}




