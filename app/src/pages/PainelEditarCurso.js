import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

import { fetchCurso, updateCurso } from '../actions/cursos'

import Loading from '../components/Loading'

@connect((store) => {
	return {
		curso: store.cursos.active.item,
	}
})
export default class PainelEditarCurso extends Component {
	constructor(props) {
		super(props)

		this.id = this.props.params.id

		this.state = {
			loading: false,

			curso: {
				nome: '',
				modulos: 2
			}
		}

		this.handleNomeChange 			= this.handleNomeChange.bind(this)
		this.handleModulosChange 		= this.handleModulosChange.bind(this)
		this.handleSubmit 				= this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this.props.dispatch(fetchCurso(this.id))
			.then((res) => {
				let data = res.action.payload.data
				this.setState({ curso: data })
			})
	}

	handleNomeChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { curso: { nome: { $set: value } } }))
	}

	handleModulosChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { curso: { modulos: { $set: value } } }))
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })
		
		this.props.dispatch(updateCurso(this.id, this.state.curso))
			.then((res) => {
				if (res.action.type === 'UPDATE_CURSO_SUCCESS') {
					this.closeModal()
				} else if (res.action.type === 'UPDATE_CURSO_ERROR') {
					this.setState({ loading: false })
					alert('Não foi possível realizar a atualização.')
				}
			})
	}

	closeModal() {
		return location.href = this.props.returnTo
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Editar Curso</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --big">
									<label>Nome</label>
									<input type="text" value={this.state.curso.nome} onChange={this.handleNomeChange} required/>
								</div>
								<div className="input --big">
									<label>Módulos</label>
									<select value={this.state.curso.modulos || ''} onChange={this.handleModulosChange} required>
										<option disabled>Selecione uma opção</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
									</select>								</div>
								<button className="btn btn-alpha" type="submit">Atualizar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




