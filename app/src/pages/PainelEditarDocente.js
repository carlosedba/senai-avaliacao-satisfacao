import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

import { fetchDocente, updateDocente } from '../actions/docentes'

import Loading from '../components/Loading'

@connect((store) => {
	return {
		docente: store.docentes.active.item,
	}
})
export default class PainelEditarDocente extends Component {
	constructor(props) {
		super(props)

		this.id = this.props.params.id

		this.state = {
			loading: false,

			docente: {
				id: this.id,
				nome: '',
				telefone: '',
				nascimento: ''
			}
		}

		this.handleNomeChange 			= this.handleNomeChange.bind(this)
		this.handleTelefoneChange		= this.handleTelefoneChange.bind(this)
		this.handleNascimentoChange 	= this.handleNascimentoChange.bind(this)
		this.handleSubmit 				= this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this.props.dispatch(fetchDocente(this.id))
			.then((res) => {
				let data = res.action.payload.data
				data.nascimento = moment.utc(data.nascimento).format("YYYY-MM-DD")
				this.setState({ docente: data })
			})
	}

	handleNomeChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { docente: { nome: { $set: value } } }))
	}

	handleTelefoneChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { docente: { telefone: { $set: value } } }))
	}

	handleNascimentoChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { docente: { nascimento: { $set: value } } }))
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })

		this.props.dispatch(updateDocente(this.id, this.state.docente))
			.then((res) => {
				if (res.action.type === 'UPDATE_DOCENTE_SUCCESS') {
					this.closeModal()
				} else if (res.action.type === 'UPDATE_DOCENTE_ERROR') {
					this.setState({ loading: false })
					alert('Não foi possível realizar a atualização.')
				}
			})
	}

	closeModal() {
		return location.href = this.props.returnTo
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Editar Docente</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --big">
									<label>Nome</label>
									<input type="text" value={this.state.docente.nome} onChange={this.handleNomeChange} required/>
								</div>
								<div className="input --midmid">
									<label>Telefone</label>
									<input type="text" value={this.state.docente.telefone} onChange={this.handleTelefoneChange} />
								</div>
								<div className="input --midmid">
									<label>Data de Nascimento</label>
									<input type="date" value={this.state.docente.nascimento} onChange={this.handleNascimentoChange} />
								</div>
								<button className="btn btn-alpha" type="submit">Atualizar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




