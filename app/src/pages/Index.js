import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { fetchOption } from '../actions/options'

import BuscaAvaliacoes from '../components/BuscaAvaliacoes'

import senai_azul from '../assets/img/senai_azul.png'
import logo_satisfacao_02 from '../assets/img/logo_satisfacao_02.png'


@connect((store) => {
	return {}
})
export default class Index extends Component {
	constructor(props) {
		super(props)

		this.state = {
			index_satisfacao: null,
		}
	}

	componentDidMount() {
		this._fetchOption('index_satisfacao')
			.then((option) => {
				let value = JSON.parse(option.value)
				this.setState({ index_satisfacao: value })
			})
	}

	_fetchOption(name) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchOption(name))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_OPTION_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	render() {
		return (
			<div>
				<main className="pagina-index">
					{(this.state.index_satisfacao !== null) ?
						(this.state.index_satisfacao) ? (
							<section className="column nowrap center-x">
								<div className="logo">
									<img src={logo_satisfacao_02} />
								</div>
								<div className="busca-avaliacoes-wrapper">
									<BuscaAvaliacoes></BuscaAvaliacoes>
								</div>
							</section>
						) : (
							<section className="column nowrap center-x">
								<div className="logo">
									<img src={senai_azul} />
								</div>
							</section>
						)
					: ''}
				</main>
			</div>
		)
	}
}




