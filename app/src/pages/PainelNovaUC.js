import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import { fetchCursos } from '../actions/cursos'
import { createUC } from '../actions/ucs'

import Loading from '../components/Loading'

@connect((store) => {
	return {
		cursos: store.cursos.list,
	}
})
export default class PainelNovaUC extends Component {
	constructor(props) {
		super(props)

		this.id = getRandomInt(1000, 9999)

		this.state = {
			loading: false,

			cursos: [],
			uc: {
				id: this.id,
				id_curso: '',
				nome: ''
			}
		}

		this.handleNomeChange 		 = this.handleNomeChange.bind(this)
		this.handleIdCursoChange	 = this.handleIdCursoChange.bind(this)
		this.handleSubmit			 = this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this.props.dispatch(fetchCursos())
			.then((res) => {
				let data = res.action.payload.data
				this.setState(update(this.state, { cursos: { $set: data } }))
			})
	}

	handleNomeChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { uc: { nome: { $set: value } } }))
	}

	handleIdCursoChange(event) {
		let value = event.target.value
		this.setState(update(this.state, { uc: { id_curso: { $set: value } } }))
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })

		this.props.dispatch(createUC(this.state.uc))
			.then((res) => {
				if (res.action.type === 'CREATE_UC_SUCCESS') {
					this.closeModal()
				} else if (res.action.type === 'CREATE_UC_ERROR') {
					this.setState({ loading: false })
					alert('Não foi possível realizar o cadastro.')
				}
			})
	}

	closeModal() {
		return location.href = this.props.returnTo
	}
	
	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Nova Unidade Curricular</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --big">
									<label>Nome</label>
									<input type="text" value={this.state.uc.nome} onChange={this.handleNomeChange} required/>
								</div>
								<div className="input --big">
									<label>Curso</label>
									<select defaultValue="true" value={this.state.uc.id_curso} onChange={this.handleIdCursoChange} required>
										<option disabled value>Selecione uma opção</option>
										{this.state.cursos.map((el, i) => {
											return <option key={i} value={el.id}>{el.nome}</option>
										})}
									</select>
								</div>
								<button className="btn btn-alpha" type="submit">Cadastrar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




