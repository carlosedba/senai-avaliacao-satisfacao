import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { logout } from '../utils/auth'


@connect((store) => {
	return {}
})
export default class Logout extends Component {
	constructor(props) {
		super(props)

		this._logout()
	}

	_logout() {
		logout()
		browserHistory.push('/')
	}

	render() {
		return null
	}
}

