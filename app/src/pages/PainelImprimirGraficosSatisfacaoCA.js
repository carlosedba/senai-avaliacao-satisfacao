import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import Grafico from '../components/Grafico'

import { fetchDTUC } from '../actions/dtucs'
import { fetchSatisfacoesPorDTUC } from '../actions/satisfacao'
import { fetchAlunosTurmaByTurma } from '../actions/alunosTurma'

import senai_cinza from '../assets/img/senai_cinza.png'

@connect((store) => {
	return {
		avaliacoes: store.satisfacao.list,
	}
})
export default class PainelImprimirGraficosSatisfacao extends Component {
	constructor(props) {
		super(props)

		this.id = this.props.params.id

		this.state = {
			dtuc: 					null,
			avaliacoes: 			[],
			alunos: 				[],
			alunosAvaliacao: 		{},

			graficos: {
				objetivoEConteudo: 				[],
				docentesTecnicosDeEnsino: 		[],
				recursosDidaticos: 				[],
				infraestrutura: 				[],
			},
		}

		this.renderGraficos = this.renderGraficos.bind(this)
		this.handleGoBack = this.handleGoBack.bind(this)
		this.handlePrint = this.handlePrint.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this._fetchDTUC(this.id)
			.then((dtuc) => {
				this.setState({ dtuc: dtuc })

				this._fetchAlunosTurmaByTurma(dtuc.id_turma)
					.then((data) => {
						this.setState({ alunos: data })
					})
			})

		this._fetchSatisfacoesPorDTUC(this.id)
			.then((avaliacoes) => {
				avaliacoes.map((el, i) => {
					el.resultado = JSON.parse(el.resultado)

					this.setState(update(this.state, { avaliacoes: { $push: [el] } }))
					this.setState(update(this.state, { alunosAvaliacao: { [el.id_aluno]: { $set: el }  } }))
				})

				this.gerarDadosGraficos()
			})
	}

	_fetchDTUC(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchDTUC(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_DTUC_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchSatisfacoesPorDTUC(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchSatisfacoesPorDTUC(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_SATISFACOES_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	_fetchAlunosTurmaByTurma(id) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchAlunosTurmaByTurma(id))
			.then((res) => {
				let type = res.action.type
				let data = res.action.payload.data
				
				;(type === 'FETCH_ALUNOS_TURMA_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})

		})
	}

	_contarPontuacao() {
		return new Promise((resolve, reject) => {
			let pontuacao = {
				objetivoEConteudo: 				0,
				docentesTecnicosDeEnsino: 		0,
				recursosDidaticos: 				0,
				infraestrutura: 				0,
			}

			let naoSeAplica = {
				objetivoEConteudo: 				0,
				docentesTecnicosDeEnsino: 		0,
				recursosDidaticos: 				0,
				infraestrutura: 				0,
			}

			this.state.avaliacoes.map((el, i) => {
				let avaliacao = el.resultado

				Object.keys(avaliacao).map((area) => {
					if (area !== 'observacoes') {
						Object.keys(avaliacao[area]).map((item) => {
							if (avaliacao[area][item]) {
								let pontos = parseInt(avaliacao[area][item])
								let pontosNaoSeAplica = (pontos === 0) ? 1 : 0

								let pontuacaoAtual = pontuacao[area]
								let novaPontuacao = pontuacaoAtual + pontos

								let naoSeAplicaAtual = naoSeAplica[area]
								let novoNaoSeAplica = naoSeAplicaAtual + pontosNaoSeAplica

								pontuacao = update(pontuacao, { [area]: { $set: novaPontuacao } })
								naoSeAplica = update(naoSeAplica, { [area]: { $set: novoNaoSeAplica } })
							}
						})
					}
				})
			})

			resolve({ pontuacao, naoSeAplica })
		})
	}

	_calcularPontuacao(data) {
		return new Promise((resolve, reject) => {
			const { pontuacao, naoSeAplica } = data

			let numeroDeAvaliacoes = this.state.avaliacoes.length

			let mediaPontuacao = {
				objetivoEConteudo: 				0,
				docentesTecnicosDeEnsino: 		0,
				recursosDidaticos: 				0,
				infraestrutura: 				0,
			}

			let mediaNaoSeAplica = {
				objetivoEConteudo: 				0,
				docentesTecnicosDeEnsino: 		0,
				recursosDidaticos: 				0,
				infraestrutura: 				0,
			}

			Object.keys(pontuacao).map((area) => {
				let mediaAreaPontuacao = 0
				let mediaAreaNaoSeAplica = 0
				
				switch (area) {
					case 'objetivoEConteudo':
						mediaAreaPontuacao = (pontuacao[area] / (10 * numeroDeAvaliacoes)) * 100
						mediaAreaNaoSeAplica = (naoSeAplica[area] / (2 * numeroDeAvaliacoes)) * 100
						break
					case 'docentesTecnicosDeEnsino':
						mediaAreaPontuacao = (pontuacao[area] / (25 * numeroDeAvaliacoes)) * 100
						mediaAreaNaoSeAplica = (naoSeAplica[area] / (5 * numeroDeAvaliacoes)) * 100
						break
					case 'recursosDidaticos':
						mediaAreaPontuacao = (pontuacao[area] / (10 * numeroDeAvaliacoes)) * 100
						mediaAreaNaoSeAplica = (naoSeAplica[area] / (2 * numeroDeAvaliacoes)) * 100
						break
					case 'infraestrutura':
						mediaAreaPontuacao = (pontuacao[area] / (15 * numeroDeAvaliacoes)) * 100
						mediaAreaNaoSeAplica = (naoSeAplica[area] / (3 * numeroDeAvaliacoes)) * 100
						break
				}

				mediaPontuacao = update(mediaPontuacao, { [area]: { $set: mediaAreaPontuacao } })
				mediaNaoSeAplica = update(mediaNaoSeAplica, { [area]: { $set: mediaAreaNaoSeAplica } })
			})

			resolve({ mediaPontuacao, mediaNaoSeAplica })
		})
	}

	gerarDadosGraficos() {
		this._contarPontuacao()
			.then(this._calcularPontuacao.bind(this))
				.then((data) => {
					const { mediaPontuacao, mediaNaoSeAplica } = data

					Object.keys(mediaPontuacao).map((area) => {
						let satisfatorio = mediaPontuacao[area]
						let naoSeAplica = mediaNaoSeAplica[area]
						let insatisfatorio = 100 - satisfatorio - naoSeAplica

						let data = [satisfatorio, insatisfatorio, naoSeAplica]

						this.setState(update(this.state, { graficos: { [area]: { $set: data } } }))
					})
				})
				.then(() => {
					setTimeout(() => {
						window.print()
					}, 1000)
				})
	}

	renderGraficos() {
		return Object.keys(this.state.graficos).map((area, i) => {
			if (this.state.graficos[area].length === 3) {
				let nome = ''
				let dados = this.state.graficos[area]

				switch (area) {
					case 'objetivoEConteudo':
						nome = 'Objetivo e Conteúdo'
						break
					case 'docentesTecnicosDeEnsino':
						nome = 'Docentes/Técnicos de Ensino'
						break
					case 'recursosDidaticos':
						nome = 'Recursos Didáticos'
						break
					case 'infraestrutura':
						nome = 'Infraestrutura'
						break
				}

				//return <Grafico key={i} type="doughnut" nome={nome} dados={dados} width="350" heigth="350" />
				return <Grafico key={i} type="pie" nome={nome} dados={dados} width="325" heigth="325" />
			}
		})
	}

	handleAvaliacaoAluno(aluno) {
		let avaliacao = this.state.alunosAvaliacao[aluno]
		if (avaliacao) {
			browserHistory.push({
				pathname: `/painel/satisfacao/${this.id}/${avaliacao.id}`,
				state: { modal: true, returnTo: this.props.location.pathname }
			})
		}
	}

	handleGoBack(event) {
		return browserHistory.goBack()
	}

	handlePrint(event) {
		return window.print()
	}

	render() {
		return (
			<div>
				<main className="pagina-impressao">
					<nav className="panel-printbar">
						<div className="panel-printbar-left">
							<ul>
								<li><button className="btn btn-alpha" onClick={this.handleGoBack}>Voltar</button></li>
							</ul>
						</div>
						<div className="panel-printbar-right">
							<ul>
								<li><button className="btn btn-alpha" onClick={this.handlePrint}>Imprimir</button></li>
							</ul>
						</div>
					</nav>
					<div className="panel-header --with-line">
						<div className="panel-header-left">
							<div className="panel-titles">
								<h1 className="panel-title">{(this.state.dtuc) && `${this.state.dtuc.docente.nome}`}</h1>
								<h2 className="panel-subtitle">{(this.state.dtuc) && `${this.state.dtuc.turma.nome} - ${this.state.dtuc.uc.nome}`}</h2>
								<h3 className="panel-lowertitle">SENAI BOQUEIRÃO</h3>
							</div>
						</div>
						<div className="panel-header-right">
							<img className="logo-senai-imprimir" src={senai_cinza} />
						</div>
					</div>
					<section className="ver-satisfacao">
						<section className="column nowrap space-between">
							<span className="section-title-alpha">Gráficos</span>
							<div className="satisfacao-graficos">
								{this.renderGraficos()}
							</div>
						</section>
					</section>
				</main>
			</div>
		)
	}
}




