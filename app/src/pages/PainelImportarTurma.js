import { SOCKETS_SERVER } from '../constants/Globals'

import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import async from 'async'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import Loading from '../components/Loading'


@connect((store) => {
	return {}
})
export default class PainelImportarTurma extends Component {
	constructor(props) {
		super(props)

		this.socket = io(SOCKETS_SERVER)

		this.state = {
			loading: false,

			xls: 'Selecionar Arquivo',
			file: null,
		}

		this.handleFileChange 			= this.handleFileChange.bind(this)
		this.handleSubmit 				= this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	handleFileChange(event) {
		let file = event.target.files[0]
		this.setState({ xls: file.name })
		this.setState({ file: file })
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })
		
		let stream = ss.createStream()
		ss.createBlobReadStream(this.state.file).pipe(stream)
		ss(this.socket).emit('cl_xlsx_alunos', stream, { size: this.state.file.size })

		this.socket.on('sv_xlsx_alunos', (alunos) => {
			browserHistory.push({
				pathname: this.props.returnTo,
				state: { import: true, data: alunos }
			})
		})
	}

	closeModal() {
		return location.href = this.props.returnTo
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Importar Turma</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --file">
									<label>{this.state.xls}</label>
									<input type="file" accept=".xls,.xlsx" onChange={this.handleFileChange} required/>
								</div>
								<button className="btn btn-alpha" type="submit">Importar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




