import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

import { fetchCursos } from '../actions/cursos'
import { fetchUC, updateUC } from '../actions/ucs'

import Loading from '../components/Loading'

@connect((store) => {
	return {
		cursos: store.cursos.list.items,
		uc: store.ucs.active.item,
	}
})
export default class PainelEditarUC extends Component {
	constructor(props) {
		super(props)

		this.id = this.props.params.id

		this.state = {
			loading: false,

			cursos: [],
			uc: {
				nome: '',
				id_curso: ''
			}
		}

		this.handleNomeChange 			= this.handleNomeChange.bind(this)
		this.handleIdCursoChange 		= this.handleIdCursoChange.bind(this)
		this.handleSubmit 				= this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this.props.dispatch(fetchCursos())
			.then((res) => {
				let data = res.action.payload.data
				this.setState({ cursos: data })
			})
		this.props.dispatch(fetchUC(this.id))
			.then((res) => {
				let data = res.action.payload.data
				this.setState({ uc: data })
			})
	}

	handleNomeChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { uc: { nome: { $set: value } } }))
	}

	handleIdCursoChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { uc: { id_curso: { $set: value } } }))
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })

		this.props.dispatch(updateUC(this.id, this.state.uc))
			.then((res) => {
				if (res.action.type === 'UPDATE_UC_SUCCESS') {
					this.closeModal()
				} else if (res.action.type === 'UPDATE_UC_ERROR') {
					this.setState({ loading: false })
					alert('Não foi possível realizar a atualização.')
				}
			})
	}

	closeModal() {
		return location.href = this.props.returnTo
	}
	
	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Editar UC</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --big">
									<label>Nome</label>
									<input type="text" value={this.state.uc.nome} onChange={this.handleNomeChange} required/>
								</div>
								<div className="input --big">
									<label>Curso</label>
									<select value={this.state.uc.id_curso} onChange={this.handleIdCursoChange} required>
										<option disabled value>Selecione uma opção</option>
										{this.state.cursos.map((el, i) => {
											return <option key={i} value={el.id}>{el.nome}</option>
										})}
									</select>
								</div>
								<button className="btn btn-alpha" type="submit">Atualizar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




