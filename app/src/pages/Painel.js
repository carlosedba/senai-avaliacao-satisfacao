import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

@connect((store) => {
	return {}
})
export default class Painel extends Component {
	constructor(props) {
		super(props)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
		browserHistory.push('/painel/satisfacao')
	}

	render() {
		return (
			<div>
				<main className="pagina-painel-index">
					<section className="row wrap">
						<div className="panel-card"></div>

						<Link to="/painel/satisfacao" className="panel-item">
							<span className="panel-item-name">Avaliações de Satisfação</span>
						</Link>
						<Link to="/painel/alunos" className="panel-item">
							<span className="panel-item-name">Alunos</span>
						</Link>
						<Link to="/painel/docentes" className="panel-item">
							<span className="panel-item-name">Docentes</span>
						</Link>
						<Link to="/painel/turmas" className="panel-item">
							<span className="panel-item-name">Turmas</span>
						</Link>
						<Link to="/painel/cursos" className="panel-item">
							<span className="panel-item-name">Cursos</span>
						</Link>
						<Link to="/painel/ucs" className="panel-item">
							<span className="panel-item-name">Unidades Curriculares</span>
						</Link>
					</section>
				</main>
			</div>
		)
	}
}




