import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'
import { getRandomInt } from '../utils/math'

import { createCurso } from '../actions/cursos'

import Loading from '../components/Loading'

@connect((store) => {
	return {}
})
export default class PainelNovoCurso extends Component {
	constructor(props) {
		super(props)

		this.id = getRandomInt(1000, 9999)

		this.state = {
			loading: false,

			curso: {
				id: this.id,
				nome: '',
				//modulos: ''
			}
		}

		this.handleNomeChange			 = this.handleNomeChange.bind(this)
		this.handleModulosChange		 = this.handleModulosChange.bind(this)
		this.handleSubmit				 = this.handleSubmit.bind(this)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	handleNomeChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { curso: { nome: { $set: value } } }))
	}

	handleModulosChange(event) {
		let value = event.target.value
		return this.setState(update(this.state, { curso: { modulos: { $set: value } } }))
	}

	handleSubmit(event) {
		event.preventDefault()

		this.setState({ loading: true })

		this.props.dispatch(createCurso(this.state.curso))
			.then((res) => {
				if (res.action.type === 'CREATE_CURSO_SUCCESS') {
					this.closeModal()
				} else if (res.action.type === 'CREATE_CURSO_ERROR') {
					this.setState({ loading: false })
					alert('Não foi possível realizar o cadastro.')
				}
			})
	}

	closeModal() {
		return location.href = this.props.returnTo
	}

	render() {
		return (
			<div>
				<form onSubmit={this.handleSubmit}>
					<Loading active={this.state.loading} />
					<div className="column nowrap">
						<div className="modal-header">
							<span className="modal-title">Novo Curso</span>
						</div>
						<div className="modal-content">
							<div className="modal-inputs">
								<div className="input --big">
									<label>Nome</label>
									<input type="text" value={this.state.curso.nome} onChange={this.handleNomeChange} required/>
								</div>
								<div className="input --big">
									<label>Módulos</label>
									<select value={this.state.curso.modulos || ''} onChange={this.handleModulosChange} required>
										<option disabled value>Selecione uma opção</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
									</select>
								</div>
								<button className="btn btn-alpha" type="submit">Cadastrar</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}




