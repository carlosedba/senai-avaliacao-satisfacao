import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { isLogged } from '../utils/auth'

import { fetchTurmas, deleteTurma } from '../actions/turmas'

@connect((store) => {
	return {
		turmas: store.turmas.list.items,
	}
})
export default class PainelTurmas extends Component {
	constructor(props) {
		super(props)
	}

	componentWillMount() {
		if (!isLogged()) browserHistory.push('/login')
	}

	componentDidMount() {
		this.props.dispatch(fetchTurmas())
			.then((res) => {
				let data = res.action.payload.data
				
				data.forEach((data, ind, arr) => {
					data.data_inicio = moment.utc(data.data_inicio).format("DD/MM/YYYY")
					data.data_conclusao = moment.utc(data.data_conclusao).format("DD/MM/YYYY")
				})

				$("#jsGrid").jsGrid({
					width: "100%",
					height: "400px",
					visible: true,

					filtering: false,
					inserting: false,
					editing: false,
					sorting: true,
					paging: true,

					noDataContent: "Nada encontrado.",
					confirmDeleting: false,
					deleteConfirm: "Tem certeza que deseja deletar este item?",
					pageIndex: 1,
					pageSize: 5,
					pageButtonCount: 15,
					pagerFormat: "Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
					pageNextText: "Próxima",
					pagePrevText: "Anterior",
					pageFirstText: "&laquo;",
					pageLastText: "&raquo;",
					pageNavigatorNextText: "...",
					pageNavigatorPrevText: "...",
					invalidMessage: "Dados Inválidos!",
					loadIndication: true,
					loadIndicationDelay: 500,
					loadMessage: "",
					loadShading: true,

					autoload: true,
					pageLoading: false,

					data: data,

					/*
					controller: {
						loadData: this._loadData,
					},
					*/

					fields: [
						{ name: "id", 			title: "ID", 				type: "text", 	width: 50 },
						{ name: "nome",			title: "Nome", 				type: "text", 	width: 200 },
						{ name: "curso.nome",	title: "Curso", 			type: "text", 	width: 200 },
						{ name: "data_inicio",	title: "Início", 			type: "text", 	width: 100 },
						{ name: "data_conclusao",title: "Conclusão", 		type: "text", 	width: 100 },
						{ type: "control", 		width: 85, itemTemplate: (value, item) => {
							var result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments)

							var editar = document.createElement('button')
							editar.addEventListener('click', this.handleEdit.bind(this, item))
							editar.innerHTML = 'Editar'
							editar.setAttribute('class', 'btn btn-grid')

							var excluir = document.createElement('button')
							excluir.addEventListener('click', this.handleDelete.bind(this, item))
							excluir.innerHTML = 'Excluir'
							excluir.setAttribute('class', 'btn btn-grid')

							var wrapper = document.createElement('div')
							wrapper.setAttribute('class', 'panel-grid-buttons')
							wrapper.appendChild(editar)
							wrapper.appendChild(excluir)

							return result.add(wrapper)
						}}
					]
				})
			})
	}

	_loadData(filter) {
		return new Promise((resolve, reject) => {
			this.props.dispatch(fetchTurmas())
				.then((res) => {
					let type = res.action.type
					let data = {
						data: res.action.payload.data,
						itemsCount: res.action.payload.data.length,
					}
					
					;(type === 'FETCH_TURMAS_SUCCESS') ? resolve(data) : reject(`Error fetching data!`)
			})
		})
	}

	handleEdit(item) {
		browserHistory.push({
			pathname: `/painel/turmas/editar/${item.id}`,
		})
	}

	handleDelete(item) {
		if (confirm('Tem certeza que deseja deletar este item?')) {
			this.props.dispatch(deleteTurma(item.id))
				.then((res) => {
					if (res.action.type === 'DELETE_TURMA_SUCCESS') {
						let data = res.action.payload.data
						let error = data.error

						if (error) {
							alert(`Erro ${error.code} - ${error.message}`)
						} else {
							$("#jsGrid").jsGrid("deleteItem", item)
						}
					} else {
						alert(`Erro 5008 - Problema na comunicação com o servidor`)
					}
				})
		}
	}

	render() {
		return (
			<div>
				<main className="pagina-painel">
					<div className="panel-header">
						<div className="panel-header-left">
							<div className="panel-titles">
								<h1 className="panel-title --with-button">Turmas</h1>
							</div>
							<Link className="btn btn-alpha"
								to={{ pathname: '/painel/turmas/novo' }}>Nova Turma</Link>
						</div>
						<div className="panel-header-right"></div>
					</div>
					<div className="panel-grid">
						<div id="jsGrid"></div>
					</div>
				</main>
			</div>
		)
	}
}




