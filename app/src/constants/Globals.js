export const DEV_BUILD = false
export const BROWSER_HISTORY = true

export const SERVER_ADDR = (!DEV_BUILD) ? location.origin : 'http://localhost:2000'
export const SOCKETS_SERVER = `${SERVER_ADDR}`

export const API_ACTIVE = true
export const API_ENDPOINT = `${SERVER_ADDR}/api/v1`


