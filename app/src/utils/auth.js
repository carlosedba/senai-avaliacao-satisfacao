import axios from 'axios'
import decode from 'jwt-decode'

import { API_ACTIVE, API_ENDPOINT } from '../constants/Globals'


module.exports = {
	isLogged() {
		let token = store.get('token')

		if (token) {
			return token
		} else {
			return false
		}
	},

	renewToken(callback) {
		let token = store.get('token')
		let decoded = decode(token)

		axios({
			method: 'get',
			url: `${API_ENDPOINT}/token/renew`,
			headers: {
				'X-Access-Token': token
			},
		})
		.then((res) => {
			if (!res.data.error) {
				let data = res.data
				
				store.set('token', data.token)
				if (callback) callback(null, data)
			} else {
				if (callback) callback(res.data.error, null)
			}
		})
	},

	decodeToken() {
		let token = store.get('token')

		if (token) {
			return decode(token)
		} else {
			return false
		}
	},

	login(credentials, callback) {
		axios({
			method: 'post',
			data: credentials,
			url: `${API_ENDPOINT}/token`,
			headers: []
		})
		.then((res) => {
			if (!res.data.error) {
				let data = res.data
				
				store.set('token', data.token)
				if (callback) callback(null, data)
			} else {
				if (callback) callback(res.data.error, null)
			}
		})
	},

	logout() {
		return store.remove('token')
	},

}