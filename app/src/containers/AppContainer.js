import { connect } from 'react-redux'
import { fetchGlobals } from '../actions/globals'
import App from '../components/App'


const mapStateToProps = (state) => {
	return {
		globals: state.globals.globals
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		fetchGlobals: () => {
			dispatch(fetchGlobals())
		}
	}
}


const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App)

export default AppContainer