import { DEV_BUILD } from '../constants/Globals'

import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'

import reducer from '../reducers'

export default function setStore(initialState) {
	const middleware = (DEV_BUILD)
		? applyMiddleware(logger(), thunk, promise({ promiseTypeSuffixes: ['LOADING', 'SUCCESS', 'ERROR'] }))
		: applyMiddleware(thunk, promise({ promiseTypeSuffixes: ['LOADING', 'SUCCESS', 'ERROR'] }))
	const composeEnhancers = (DEV_BUILD) ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose
	const store = createStore(reducer, initialState, composeEnhancers(middleware))

	if (DEV_BUILD && module.hot) {
		module.hot.accept('../reducers', () => {
			store.replaceReducer(require('../reducers'))
		})
	}

	return store
}