import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { search } from '../actions/search'

import user from '../assets/img/user.png'


@connect((store) => {
	return {
		searchResults: 		store.search.results,
	}
})
export default class BuscaPainel extends Component {
	constructor(props) {
		super(props)

		this.component = null
		this.input = null

		this.state = {
			search: '',
			searchResults: [],
			focusedResultIndex: -1,
		}

		this.handleSearchChange		 = this.handleSearchChange.bind(this)
		this.moveCursorToEnd		 = this.moveCursorToEnd.bind(this)
		this.goToResult				 = this.goToResult.bind(this)
	}

	componentDidMount() {
		this.component.addEventListener('keydown', this.handleKeyDown.bind(this))
	}

	moveCursorToEnd(e) {
		let value = e.target.value

		e.target.value = ''
		e.target.value = value
	}

	handleSearchChange(event) {
		let value = event.target.value

		this.setState({ search: value })
		this.setState({ focusedResultIndex: -1 })

		this.search(value)
	}

	handleKeyDown(event) {
		switch (event.keyCode) {
			case 13:
				//this.goToResult()
				break
			case 38:
				if (this.state.searchResults.length > 0) {}
				break
			case 40:
				if (this.state.searchResults.length > 0) {}
				break
		}

	}

	search(query) {
		if (query.length > 0) {
			this.props.dispatch(search({
				query: query,
				include: ['alunos', 'professores', 'cursos', 'turmas']
			}))
			.then((res) => {
				let data = res.action.payload.data
				console.log(data)
				this.setState({ searchResults: data })
			})
		} else {
			setTimeout(() => { this.setState({ searchResults: [] }) })
		}
	}

	goToResult(event) {
		if (this.state.searchResults.length > 0) {
			if (this.state.focusedResultIndex !== -1) {
				let results = document.querySelectorAll('.topbar-search-results li a')
				let focusedResult = results[this.state.focusedResultIndex]
				location.href = focusedResult.href
			}
		}
	}

	render() {
		return (
			<div ref={(el) => { this.component = el }}>
				<div className="topbar-search">
					<input type="text" placeholder="Busca..." ref={(input) => this.input = input} value={this.state.search} onChange={this.handleSearchChange} onFocus={this.moveCursorToEnd} />
				</div>
				<div className="topbar-search-results">
					<div className="topbar-search-section">
						<span className="topbar-search-section-title">Alunos</span>
						<div className="topbar-search-results-wrapper">
							<div className="topbar-search-result --aluno">
								<img className="topbar-search-image-placeholder" src={user} />
								<span className="topbar-search-name">Carlos Eduardo</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}




