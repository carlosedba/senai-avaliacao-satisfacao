import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'


export default class Grid extends Component {
	constructor(props) {
		super(props)
	}

	componentDidMount() {
		$('#grid').jsGrid({
			width: 			this.props.width,
			height: 		this.props.height,
			visible: 		this.props.visible,

			filtering: 		false,
			inserting: 		false,
			editing: 		false,
			sorting: 		true,
			paging: 		true,

			noDataContent: 				"Nada encontrado.",
			confirmDeleting: 			false,
			deleteConfirm: 				"Tem certeza que deseja deletar este item?",
			pageIndex: 					1,
			pageSize: 					5,
			pageButtonCount: 			15,
			pagerFormat: 				"Páginas: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; {pageIndex} de {pageCount}",
			pageNextText: 				"Próxima",
			pagePrevText: 				"Anterior",
			pageFirstText: 				"Primeira",
			pageLastText: 				"Última",
			pageNavigatorNextText: 		"...",
			pageNavigatorPrevText: 		"...",
			invalidMessage: 			"Dados Inválidos!",
			loadIndication: 			true,
			loadIndicationDelay: 		500,
			loadMessage: 				"",
			loadShading: 				true,

			data: 		this.props.data,
			fields: 	this.props.fields,
		})
	}

	render() {
		return (	
			<div>
				<div id="grid"></div>
			</div>
		)
	}
}

