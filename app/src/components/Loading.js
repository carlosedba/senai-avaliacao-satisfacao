import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'
import LoadingAnimation from 'react-loading'


export default class Loading extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<div className={(this.props.active) ? 'loading-wrapper' : 'hidden'}>
				<div className="loading-background"></div>
				<LoadingAnimation type="spin" color="rgba(255, 255, 255, 0.75)" />
			</div>
		)
	}
}




