import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'


export default class Grafico extends Component {
	constructor(props) {
		super(props)

		this.grafico = null

		this.state = {}
	}

	componentDidMount() {
		let data = {
			labels: [
				"Satisfatório",
				"Insatisfatório",
				"Não se aplica",
			],
			datasets: [{
				data: this.props.dados,
				backgroundColor: [
					"#006CAD",
					"#00B0FF",
					"#FF9100",
				],
				hoverBackgroundColor: [
					"#006CAD",
					"#00B0FF",
					"#FF9100",
				],
				hoverBorderColor: [
					"#006CAD",
					"#00B0FF",
					"#FF9100",
				],
			}]
		}

		const canvas = this.grafico.getContext('2d')
		const Grafico = new Chart(canvas, {
			type: this.props.type,

			data: data,

			options: {
				responsive: 		false,

				events: false,

				title: {
					display: 		false,
					position: 		'top',
					fullWidth: 		true,
					fontSize: 		12,
					fontFamily: 	'Roboto',
					fontColor: 		'#666',
					fontStyle: 		'bold',
					padding: 		10,
					text: 			this.props.nome,
				},

				legend: {
					display: 		true,
					position: 		'left',
					labels: {
						boxWidth: 		30,
						padding: 		15,
					},
				},

				tooltips: {
					mode: 					'nearest',
					bodyFontFamily: 		'Roboto',
					displayColors: 			false,

					callbacks: {
						label: function (tooltipItem, data) {
							var dataset = data.datasets[tooltipItem.datasetIndex]
							var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
								return previousValue + currentValue
							})
							var currentValue = dataset.data[tooltipItem.index]
							var precentage = Math.floor(((currentValue/total) * 100) + 0.5)  

							return precentage + "%";
						},
					},
				},

				animation: {
					//onProgress: this.drawSegmentValues.bind(this, canvas, data)
					onComplete: function () {
						var self = this,
						chartInstance = this.chart,
						ctx = chartInstance.ctx;

						ctx.font = '400 16px Roboto';
						ctx.textAlign = "center";
						ctx.fillStyle = "#ffffff";

						Chart.helpers.each(self.data.datasets.forEach((dataset, datasetIndex) => {
						var meta = self.getDatasetMeta(datasetIndex),
							total = 0, //total values to compute fraction
							labelxy = [],
							offset = Math.PI / 2, //start sector from top
							radius,
							centerx,
							centery, 
							lastend = 0; //prev arc's end line: starting with 0

						for (var val of dataset.data) { total += val; } 

						Chart.helpers.each(meta.data.forEach((element, index) => {
							radius = 0.9 * element._model.outerRadius - element._model.innerRadius;
							centerx = element._model.x;
							centery = element._model.y;
							var thispart = dataset.data[index],
							arcsector = Math.PI * (2 * thispart / total);
							if (element.hasValue() && dataset.data[index] > 0) {
								labelxy.push(lastend + arcsector / 2 + Math.PI + offset);
							}
							else {
								labelxy.push(-1);
							}
							lastend += arcsector;
						}), self)

						var lradius = radius * 3 / 4;
						for (var idx in labelxy) {
							if (labelxy[idx] === -1) continue;
								var langle = labelxy[idx],
								dx = centerx + lradius * Math.cos(langle),
								dy = centery + lradius * Math.sin(langle),
								val = Math.round(dataset.data[idx] / total * 100);
								ctx.fillText(val + '%', dx, dy);
							}

						}), self);
					}
				},
			},
		})
	}

	getTotalValue(arr) {
		let datasets = arr.datasets
		let total = 0

		datasets.map((dataset, i) => {
			dataset.data.map((el, i) => {
				total = total + el
			})
		})

		return total
	}

	drawSegmentValues(canvas, data, animation) {
		let chart = animation.chartInstance
		let midX = canvas.width / 2
		let midY = canvas.height / 2
		let totalValue = this.getTotalValue(data)
		let radius = chart.outerRadius

		console.log('always', chart)

		for (let i = 0; i < chart.segments.length; i++) {
			var textSize = canvas.width / 10

			ctx.fillStyle = 'white'
			ctx.font = `${textSize}px Roboto`

			// Get needed variables
			var value = Math.floor(chart.segments[i].value / totalValue * 100) + '%'
			var startAngle = chart.segments[i].startAngle
			var endAngle = chart.segments[i].endAngle
			var middleAngle = startAngle + ((endAngle - startAngle) / 2)

			// Compute text location
			var posX = (radius / 2) * Math.cos(middleAngle) + midX
			var posY = (radius / 2) * Math.sin(middleAngle) + midY

			// Text offside by middle
			var w_offset = ctx.measureText(value).width / 2
			var h_offset = textSize / 4

			ctx.fillText(value, posX - w_offset, posY + h_offset)
		}
	}

	render() {
		return (
			<div className="grafico">
				<span className="grafico-nome">{this.props.nome}</span>
				<canvas className="grafico-canvas"
					ref={(canvas) => { this.grafico = canvas }}
					width={this.props.width}
					height={this.props.height} />
			</div>
		)
	}
}




