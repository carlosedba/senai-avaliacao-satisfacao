import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { decodeToken } from '../utils/auth'

import BuscaPainel from './BuscaPainel'

import senai from '../assets/img/senai.png'
import eu from '../assets/img/eu.jpg'


export default class Header extends Component {
	constructor(props) {
		super(props)

		this.token = decodeToken()

		this.isCurrentPath			 = this.isCurrentPath.bind(this)
		this.getHeaderClass			 = this.getHeaderClass.bind(this)
		this.getLinkbarClass		 = this.getLinkbarClass.bind(this)
		this.getLinkClass			 = this.getLinkClass.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		this.token = decodeToken()
	}
	
	isCurrentPath(pathname) {
		return (pathname === this.props.currentPath())
	}

	getHeaderClass() {
		var currentPath = this.props.currentPath()
		var paths = currentPath.split(',')
		return ((currentPath !== '') &&
				(currentPath !== 'avaliacao-satisfacao') &&
				(currentPath !== 'login') &&
				(paths[2] !== 'i')) ? 'header' : 'header hidden'
	}

	getLinkbarClass() {
		var currentPath = this.props.currentPath()
		return (currentPath !== 'painel') ? 'navbar' : 'navbar hidden'
	}

	getLinkClass(path) {
		var currentPath = this.props.currentPath()
		currentPath = currentPath.split('/')[1]

		return (currentPath === path) ? 'active' : ''
	}

	render() {
		return (
			<header className={this.getHeaderClass()}>
				<nav className="bluebar">
					<div className="bluebar-left">
						<ul>
							<li><Link to="/">Início</Link></li>
						</ul>
					</div> 
					<div className="bluebar-center">
						<Link to="/painel" style={{ textDecoration: 'none' }}>
							<div className="bluebar-logo"><img src={senai} /></div>
							<span className="bluebar-title">Painel</span>
						</Link>
					</div>
					<div className="bluebar-right">
						<ul>
							<li>
								{/**<img className="bluebar-user-picture" src={eu} />**/}
								<span className="bluebar-username">{this.token.nome}</span>
								<ul className="submenu">
									<li><Link to="/logout">Sair</Link></li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
				<nav className={this.getLinkbarClass()}>
					<ul>
						<li className={this.getLinkClass('satisfacao')}><Link to="/painel/satisfacao">Avaliações de Satisfação</Link></li>
						<li className={this.getLinkClass('alunos')}><Link to="/painel/alunos">Alunos</Link></li>
						<li className={this.getLinkClass('docentes')}><Link to="/painel/docentes">Docentes</Link></li>
						<li className={this.getLinkClass('cursos')}><Link to="/painel/cursos">Cursos</Link></li>
						<li className={this.getLinkClass('turmas')}><Link to="/painel/turmas">Turmas</Link></li>
						<li className={this.getLinkClass('ucs')}><Link to="/painel/ucs">Unidades Curriculares</Link></li>
						<li className={this.getLinkClass('usuarios')}><Link to="/painel/usuarios">Usuários</Link></li>
					</ul>
				</nav>
			</header>
		)
	}
}




