import React, { Component } from 'react'
import { browserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'


export default class Modal extends Component {
	constructor(props) {
		super(props)

		window.addEventListener('beforeunload', (e) => {
			
		})

		this.closeModal = this.closeModal.bind(this)
	}

	componentDidMount() {}

	closeModal(event) {
		return history.back()
	} 

	render() {
		return (
			<div className="modal-wrapper">
				<div onClick={this.closeModal} className="modal-background"></div>
				<div className="modal">
					{React.cloneElement(this.props.children, { returnTo: this.props.returnTo })}
					<Link to={this.props.returnTo} className="modal-close">&#10006;</Link>
				</div>
			</div>
		)
	}
}




