import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import update from 'immutability-helper'

import { BROWSER_HISTORY } from '../constants/Globals'

import { searchAlunos } from '../actions/alunos'


@connect((store) => {
	return {
		search: 		store.alunos.search.items,
	}
})
export default class BuscaAvaliacoes extends Component {
	constructor(props) {
		super(props)

		this.component = null
		this.input = null

		this.state = {
			search: '',
			searchResults: [],
			focusedResultIndex: -1,
		}

		this.handleSearchChange		 = this.handleSearchChange.bind(this)
		this.displayResults			 = this.displayResults.bind(this)
		this.moveCursorToEnd		 = this.moveCursorToEnd.bind(this)
		this.goToResult				 = this.goToResult.bind(this)
	}

	componentDidMount() {
		this.component.addEventListener('keydown', this.handleKeyDown.bind(this))
	}

	moveCursorToEnd(e) {
		let value = e.target.value

		e.target.value = ''
		e.target.value = value
	}

	handleSearchChange(event) {
		let value = event.target.value

		this.setState({ search: value })
		this.setState({ focusedResultIndex: -1 })

		this.search(value)
	}

	handleKeyDown(event) {
		switch (event.keyCode) {
			case 13:
				this.goToResult()
				break
			case 38:
				if (this.state.searchResults.length > 0) {
					if (this.state.focusedResultIndex > -1) {
						this.setState({ focusedResultIndex: this.state.focusedResultIndex - 1 })
					}

					if (this.state.focusedResultIndex >= 0) {						
						let results = document.querySelectorAll('.results li')
						let focusedResult = results[this.state.focusedResultIndex]

						focusedResult.focus()
						this.input.dataset.userInput = this.input.value
						this.input.value = focusedResult.innerText
					}

					if (this.state.focusedResultIndex === -1) {
						this.input.focus()
						this.input.value = this.input.dataset.userInput
					}
				}
				break
			case 40:
				console.log('length', this.state.searchResults.length)
				if (this.state.searchResults.length > 0) {
					console.log(this.state.focusedResultIndex)

					if (this.state.focusedResultIndex < this.state.searchResults.length - 1) {
						this.setState({ focusedResultIndex: this.state.focusedResultIndex + 1 })
					}

					if (this.state.focusedResultIndex < this.state.searchResults.length) {						
						let results = document.querySelectorAll('.results li')
						let focusedResult = results[this.state.focusedResultIndex]

						console.log(focusedResult)

						focusedResult.focus()
						this.input.dataset.userInput = this.input.value
						this.input.value = focusedResult.innerText
					}
				}
				break
		}

		//console.log(this.state.focusedResultIndex)
	}

	search(query) {
		if (query.length > 0) {
			this.props.dispatch(searchAlunos({
				query: query
			}))
			.then((res) => {
				let data = res.action.payload.data
				this.setState({ searchResults: data })
			})
		} else {
			setTimeout(() => { this.setState({ searchResults: [] }) })
		}
	}

	displayResults(result) {
		let id = result.id
		let nome = result.nome.toLowerCase()
		let busca = this.state.search.toLowerCase()

		let fim = nome.split(busca)[1]
		let inicio = nome.substring(0, nome.indexOf(fim))
		let hidden = ''

		if (nome.indexOf(fim) <= 0) {
			hidden = 'hidden'
		}

		return (
			<li key={id} tabIndex="0" className={hidden}>
				<Link to={`/avaliacao-satisfacao/${id}`}>
					{inicio}
					<span className="bold-text">{fim}</span>
				</Link>
			</li>
		)
	}

	goToResult(event) {
		if (this.state.searchResults.length > 0) {
			if (this.state.focusedResultIndex !== -1) {
				let results = document.querySelectorAll('.results li a')
				let focusedResult = results[this.state.focusedResultIndex]
				location.href = focusedResult.href
			}
		}
	}

	render() {
		return (
			<div className="busca-avaliacoes" ref={(el) => { this.component = el }}>
				<div className="search row nowrap">
					<input className="search-input" type="text" ref={(input) => this.input = input} value={this.state.search} onChange={this.handleSearchChange} onFocus={this.moveCursorToEnd} />
					<button className="search-arrow" onClick={this.goToResult}>
						<svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
							<path d="M0 0h24v24H0z" fill="none"/>
							<path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"/>
						</svg>
					</button>
				</div>
				<div className="results">
					<ul>
						{this.state.searchResults.map((el, i) => {
							return this.displayResults(el)
						})}
					</ul>
				</div>
			</div>
		)
	}
}




