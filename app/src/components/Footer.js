import React, { Component } from 'react'
import { Link } from 'react-router'


export default class Footer extends Component {
	constructor(props) {
		super(props)

		this.isCurrentPath			 = this.isCurrentPath.bind(this)
	}
	
	isCurrentPath(pathname) {
		return (pathname === this.props.currentPath())
	}

	render() {
		return (	
			<footer className="footer">
				<span className="footer-devnote">Desenvolvido por <a href="http://carlosedba.github.io">Carlos Eduardo</a></span>
				{ (this.isCurrentPath('') ? <Link className="footer-login" to="/login">Ir para o Painel</Link> : '') }
			</footer>
		)
	}
}




