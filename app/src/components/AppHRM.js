import React, { Component } from 'react'
import { borwserHistory, Link } from 'react-router'
import { connect } from 'react-redux'
import { AppContainer } from 'react-hot-loader'
import update from 'immutability-helper'

import { BROWSER_HISTORY } from '../constants/Globals'

import { isLogged, renewToken } from '../utils/auth'

import Header from './Header'
import Modal from './Modal'
import Footer from './Footer'


@connect((store) => {
	return {}
})
export default class App extends Component {
	constructor(props) {
		super(props)

		if (module.hot) {
			module.hot.accept('./App', () => {
				this.renderWithHRM()
			})
		}

		this.getCurrentPath 			= this.getCurrentPath.bind(this)
		this.isCurrentPath 				= this.isCurrentPath.bind(this)
		this.renderWithHRM 				= this.renderWithHRM.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		if ((
			nextProps.location.state &&
			nextProps.location.state.modal
		)) {

			if (nextProps.location.key !== this.props.location.key) {
				this.previousChildren = this.props.children
			}
		}
	}

	componentWillMount() {
		const { location } = this.props

		if ((
			location.state &&
			location.state.modal
		)) {
			if (!this.previousChildren) {
				this.previousChildren = <div className="pagina-painel"></div>
			}
		}

		if (isLogged()) renewToken()
	}

	getCurrentPath() {
		var loc = (BROWSER_HISTORY) ? location.pathname.split(/\/|\?|&|=|\./g) : location.hash.split(/\/|\?|&|=|\./g)
		loc = loc.filter((val) => { return val !== '' })

		if (loc.length > 1) {
			switch (loc[0]) {
				case 'avaliacao-satisfacao':
					return loc[0]
					break
				default:
					return loc = loc.toString().replace(',', '/')
					break
			}
		} else {
			return loc.toString()
		}
	}
	
	isCurrentPath(pathname) {
		const globals = this.props.globals
		var currentPath = this.getCurrentPath()

		return (pathname === currentPath)
	}

	renderWithHRM() {
		const globals = this.props.globals

		let { location } = this.props

		let isModal = (
			location.state &&
			location.state.modal &&
			this.previousChildren
		)

		return (
			<AppContainer>
				<div className="app">

					<Header currentPath={this.getCurrentPath}></Header>

					{isModal ? this.previousChildren : this.props.children }

					{isModal && (
						<Modal isOpen={true} returnTo={location.state.returnTo}>
							{this.props.children}
						</Modal>
					)}

					<Footer currentPath={this.getCurrentPath}></Footer>
				</div>
			</AppContainer>
		)
	}

	render() {
		return this.renderWithHRM()
	}
}

