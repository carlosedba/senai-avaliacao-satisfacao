import {
	FETCH_TURMAS, FETCH_TURMAS_LOADING, FETCH_TURMAS_SUCCESS, FETCH_TURMAS_ERROR,
	FETCH_TURMA, FETCH_TURMA_LOADING, FETCH_TURMA_SUCCESS, FETCH_TURMA_ERROR,
	CREATE_TURMA, CREATE_TURMA_LOADING, CREATE_TURMA_SUCCESS, CREATE_TURMA_ERROR,
	UPDATE_TURMA, UPDATE_TURMA_LOADING, UPDATE_TURMA_SUCCESS, UPDATE_TURMA_ERROR,
	DELETE_TURMA, DELETE_TURMA_LOADING, DELETE_TURMA_SUCCESS, DELETE_TURMA_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_TURMAS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_TURMAS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_TURMAS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_TURMAS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_TURMA:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_TURMA_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_TURMA_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_TURMA:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_TURMA_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_TURMA_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_TURMA:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_TURMA_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_TURMA_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_TURMA:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_TURMA_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_TURMA_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
