import {
	SEARCH, SEARCH_LOADING, SEARCH_SUCCESS, SEARCH_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	results: { items: [], error: null, loading: null }
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case SEARCH:
		return { ...state, results: { items: [], error: null, loading: null } }

	case SEARCH_LOADING:
		return { ...state, results: { items: [], error: null, loading: true } }

	case SEARCH_SUCCESS:
		return { ...state, results: { items: action.payload.data, error: null, loading: false } }

	case SEARCH_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, results: { items: [], error: error, loading: false } }

	default:
		return state;
  }
}
