import {
	FETCH_CURSOS, FETCH_CURSOS_LOADING, FETCH_CURSOS_SUCCESS, FETCH_CURSOS_ERROR,
	FETCH_CURSO, FETCH_CURSO_LOADING, FETCH_CURSO_SUCCESS, FETCH_CURSO_ERROR,
	CREATE_CURSO, CREATE_CURSO_LOADING, CREATE_CURSO_SUCCESS, CREATE_CURSO_ERROR,
	UPDATE_CURSO, UPDATE_CURSO_LOADING, UPDATE_CURSO_SUCCESS, UPDATE_CURSO_ERROR,
	DELETE_CURSO, DELETE_CURSO_LOADING, DELETE_CURSO_SUCCESS, DELETE_CURSO_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_CURSOS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_CURSOS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_CURSOS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_CURSOS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_CURSO:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_CURSO_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_CURSO_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_CURSO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_CURSO:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_CURSO_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_CURSO_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_CURSO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_CURSO:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_CURSO_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_CURSO_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_CURSO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_CURSO:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_CURSO_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_CURSO_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_CURSO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
