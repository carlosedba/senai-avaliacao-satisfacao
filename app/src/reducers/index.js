import { combineReducers } from 'redux'

import alunos 			from './alunos'
import alunosTurma		from './alunosTurma'
import auth				from './auth'
import cursos 			from './cursos'
import docentes 		from './docentes'
import dtucs 			from './dtucs'
import globals 			from './globals'
import satisfacao 		from './satisfacao'
import search 			from './search'
import turmas 			from './turmas'
import ucs 				from './ucs'
import usuarios 		from './usuarios'

const reducer = combineReducers({
	alunos,
	alunosTurma,
	auth,
	cursos,
	docentes,
	dtucs,
	globals,
	satisfacao,
	search,
	turmas,
	ucs,
	usuarios,
})

export default reducer

