import {
	FETCH_OPTIONS, FETCH_OPTIONS_LOADING, FETCH_OPTIONS_SUCCESS, FETCH_OPTIONS_ERROR,
	FETCH_OPTION, FETCH_OPTION_LOADING, FETCH_OPTION_SUCCESS, FETCH_OPTION_ERROR,
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	options: 			{ items: [], error: null, loading: null },
	option: 			{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_OPTIONS:
		return { ...state, options: { items: [], error: null, loading: null } }

	case FETCH_OPTIONS_LOADING:
		return { ...state, options: { items: [], error: null, loading: true } }

	case FETCH_OPTIONS_SUCCESS:
		return { ...state, options: { items: action.payload.data, error: null, loading: false } }

	case FETCH_OPTIONS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, options: { items: [], error: error, loading: false } }


	case FETCH_OPTION:
		return { ...state, option: { item: null, error: null, loading: null } }

	case FETCH_OPTION_LOADING:
		return { ...state, option: { item: null, error: null, loading: true } }

	case FETCH_OPTION_SUCCESS:
		return { ...state, option: { item: action.payload.data, error: null, loading: false } }

	case FETCH_OPTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, option: { item: null, error: error, loading: false } }


	case UPDATE_OPTION:
		return { ...state, option: { item: null, error: null, loading: null } }

	case UPDATE_OPTION_LOADING:
		return { ...state, option: { item: null, error: null, loading: true } }

	case UPDATE_OPTION_SUCCESS:
		return { ...state, option: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_OPTION_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, option: { item: null, error: error, loading: false } }
		
	default:
		return state;
  }
}
