import {
	SEARCH_SATISFACOES, SEARCH_SATISFACOES_LOADING, SEARCH_SATISFACOES_SUCCESS, SEARCH_SATISFACOES_ERROR,
	FETCH_SATISFACOES, FETCH_SATISFACOES_LOADING, FETCH_SATISFACOES_SUCCESS, FETCH_SATISFACOES_ERROR,
	FETCH_SATISFACAO, FETCH_SATISFACAO_LOADING, FETCH_SATISFACAO_SUCCESS, FETCH_SATISFACAO_ERROR,
	CREATE_SATISFACAO, CREATE_SATISFACAO_LOADING, CREATE_SATISFACAO_SUCCESS, CREATE_SATISFACAO_ERROR,
	UPDATE_SATISFACAO, UPDATE_SATISFACAO_LOADING, UPDATE_SATISFACAO_SUCCESS, UPDATE_SATISFACAO_ERROR,
	DELETE_SATISFACAO, DELETE_SATISFACAO_LOADING, DELETE_SATISFACAO_SUCCESS, DELETE_SATISFACAO_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case SEARCH_SATISFACOES:
		return { ...state, search: { items: [], error: null, loading: null } }

	case SEARCH_SATISFACOES_LOADING:
		return { ...state, search: { items: [], error: null, loading: true } }

	case SEARCH_SATISFACOES_SUCCESS:
		return { ...state, search: { items: action.payload.data, error: null, loading: false } }

	case SEARCH_SATISFACOES_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, search: { items: [], error: error, loading: false } }


	case FETCH_SATISFACOES:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_SATISFACOES_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_SATISFACOES_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_SATISFACOES_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_SATISFACAO:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_SATISFACAO_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_SATISFACAO_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_SATISFACAO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_SATISFACAO:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_SATISFACAO_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_SATISFACAO_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_SATISFACAO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_SATISFACAO:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_SATISFACAO_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_SATISFACAO_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_SATISFACAO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_SATISFACAO:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_SATISFACAO_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_SATISFACAO_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_SATISFACAO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
