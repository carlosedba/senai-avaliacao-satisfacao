import {
	SEARCH_ALUNOS_TURMA, SEARCH_ALUNOS_TURMA_LOADING, SEARCH_ALUNOS_TURMA_SUCCESS, SEARCH_ALUNOS_TURMA_ERROR,
	FETCH_ALUNOS_TURMA, FETCH_ALUNOS_TURMA_LOADING, FETCH_ALUNOS_TURMA_SUCCESS, FETCH_ALUNOS_TURMA_ERROR,
	FETCH_ALUNO_TURMA, FETCH_ALUNO_TURMA_LOADING, FETCH_ALUNO_TURMA_SUCCESS, FETCH_ALUNO_TURMA_ERROR,
	CREATE_ALUNO_TURMA, CREATE_ALUNO_TURMA_LOADING, CREATE_ALUNO_TURMA_SUCCESS, CREATE_ALUNO_TURMA_ERROR,
	UPDATE_ALUNO_TURMA, UPDATE_ALUNO_TURMA_LOADING, UPDATE_ALUNO_TURMA_SUCCESS, UPDATE_ALUNO_TURMA_ERROR,
	DELETE_ALUNO_TURMA, DELETE_ALUNO_TURMA_LOADING, DELETE_ALUNO_TURMA_SUCCESS, DELETE_ALUNO_TURMA_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case SEARCH_ALUNOS_TURMA:
		return { ...state, search: { items: [], error: null, loading: null } }

	case SEARCH_ALUNOS_TURMA_LOADING:
		return { ...state, search: { items: [], error: null, loading: true } }

	case SEARCH_ALUNOS_TURMA_SUCCESS:
		return { ...state, search: { items: action.payload.data, error: null, loading: false } }

	case SEARCH_ALUNOS_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, search: { items: [], error: error, loading: false } }


	case FETCH_ALUNOS_TURMA:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_ALUNOS_TURMA_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_ALUNOS_TURMA_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_ALUNOS_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_ALUNO_TURMA:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_ALUNO_TURMA_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_ALUNO_TURMA_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_ALUNO_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_ALUNO_TURMA:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_ALUNO_TURMA_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_ALUNO_TURMA_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_ALUNO_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_ALUNO_TURMA:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_ALUNO_TURMA_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_ALUNO_TURMA_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_ALUNO_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_ALUNO_TURMA:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_ALUNO_TURMA_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_ALUNO_TURMA_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_ALUNO_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
