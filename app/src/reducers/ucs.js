import {
	FETCH_UCS, FETCH_UCS_LOADING, FETCH_UCS_SUCCESS, FETCH_UCS_ERROR,
	FETCH_UC, FETCH_UC_LOADING, FETCH_UC_SUCCESS, FETCH_UC_ERROR,
	FETCH_UC_CURSO, FETCH_UC_CURSO_LOADING, FETCH_UC_CURSO_SUCCESS, FETCH_UC_CURSO_ERROR,
	CREATE_UC, CREATE_UC_LOADING, CREATE_UC_SUCCESS, CREATE_UC_ERROR,
	UPDATE_UC, UPDATE_UC_LOADING, UPDATE_UC_SUCCESS, UPDATE_UC_ERROR,
	DELETE_UC, DELETE_UC_LOADING, DELETE_UC_SUCCESS, DELETE_UC_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_UCS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_UCS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_UCS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_UCS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_UC:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_UC_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_UC_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_UC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case FETCH_UC_CURSO:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_UC_CURSO_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_UC_CURSO_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_UC_CURSO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_UC:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_UC_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_UC_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_UC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_UC:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_UC_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_UC_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_UC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_UC:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_UC_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_UC_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_UC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
