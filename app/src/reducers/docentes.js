import {
	FETCH_DOCENTES, FETCH_DOCENTES_LOADING, FETCH_DOCENTES_SUCCESS, FETCH_DOCENTES_ERROR,
	FETCH_DOCENTE, FETCH_DOCENTE_LOADING, FETCH_DOCENTE_SUCCESS, FETCH_DOCENTE_ERROR,
	CREATE_DOCENTE, CREATE_DOCENTE_LOADING, CREATE_DOCENTE_SUCCESS, CREATE_DOCENTE_ERROR,
	UPDATE_DOCENTE, UPDATE_DOCENTE_LOADING, UPDATE_DOCENTE_SUCCESS, UPDATE_DOCENTE_ERROR,
	DELETE_DOCENTE, DELETE_DOCENTE_LOADING, DELETE_DOCENTE_SUCCESS, DELETE_DOCENTE_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_DOCENTES:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_DOCENTES_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_DOCENTES_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_DOCENTES_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_DOCENTE:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_DOCENTE_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_DOCENTE_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_DOCENTE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_DOCENTE:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_DOCENTE_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_DOCENTE_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_DOCENTE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_DOCENTE:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_DOCENTE_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_DOCENTE_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_DOCENTE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_DOCENTE:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_DOCENTE_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_DOCENTE_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_DOCENTE_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
