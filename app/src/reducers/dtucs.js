import {
	FETCH_DTUCS, FETCH_DTUCS_LOADING, FETCH_DTUCS_SUCCESS, FETCH_DTUCS_ERROR,
	FETCH_DTUCS_TURMA, FETCH_DTUCS_TURMA_LOADING, FETCH_DTUCS_TURMA_SUCCESS, FETCH_DTUCS_TURMA_ERROR,
	FETCH_DTUC, FETCH_DTUC_LOADING, FETCH_DTUC_SUCCESS, FETCH_DTUC_ERROR,
	FETCH_DTUC_CURSO, FETCH_DTUC_CURSO_LOADING, FETCH_DTUC_CURSO_SUCCESS, FETCH_DTUC_CURSO_ERROR,
	CREATE_DTUC, CREATE_DTUC_LOADING, CREATE_DTUC_SUCCESS, CREATE_DTUC_ERROR,
	UPDATE_DTUC, UPDATE_DTUC_LOADING, UPDATE_DTUC_SUCCESS, UPDATE_DTUC_ERROR,
	DELETE_DTUC, DELETE_DTUC_LOADING, DELETE_DTUC_SUCCESS, DELETE_DTUC_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	search: 		{ items: [], error: null, loading: null },
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_DTUCS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_DTUCS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_DTUCS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_DTUCS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }

	case FETCH_DTUCS_TURMA:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_DTUCS_TURMA_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_DTUCS_TURMA_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_DTUCS_TURMA_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_DTUC:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_DTUC_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_DTUC_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_DTUC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case FETCH_DTUC_CURSO:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_DTUC_CURSO_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_DTUC_CURSO_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_DTUC_CURSO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_DTUC:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_DTUC_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_DTUC_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_DTUC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_DTUC:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_DTUC_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_DTUC_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_DTUC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_DTUC:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_DTUC_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_DTUC_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_DTUC_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
