import {
	AUTH_FETCH_TOKEN, AUTH_FETCH_TOKEN_LOADING, AUTH_FETCH_TOKEN_SUCCESS, AUTH_FETCH_TOKEN_ERROR,
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	login: 		{ token: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case AUTH_FETCH_TOKEN:
		return { ...state, login: { token: null, error: null, loading: null } }

	case AUTH_FETCH_TOKEN_LOADING:
		return { ...state, login: { token: null, error: null, loading: true } }

	case AUTH_FETCH_TOKEN_SUCCESS:
		return { ...state, login: { token: action.payload.data, error: null, loading: false } }

	case AUTH_FETCH_TOKEN_ERROR:
		error = action.payload.response.data.error
		return { ...state, login: { token: null, error: error, loading: false } }

	default:
		return state;
  }
}
