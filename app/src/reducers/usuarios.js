import {
	FETCH_USUARIOS, FETCH_USUARIOS_LOADING, FETCH_USUARIOS_SUCCESS, FETCH_USUARIOS_ERROR,
	FETCH_USUARIO, FETCH_USUARIO_LOADING, FETCH_USUARIO_SUCCESS, FETCH_USUARIO_ERROR,
	CREATE_USUARIO, CREATE_USUARIO_LOADING, CREATE_USUARIO_SUCCESS, CREATE_USUARIO_ERROR,
	UPDATE_USUARIO, UPDATE_USUARIO_LOADING, UPDATE_USUARIO_SUCCESS, UPDATE_USUARIO_ERROR,
	DELETE_USUARIO, DELETE_USUARIO_LOADING, DELETE_USUARIO_SUCCESS, DELETE_USUARIO_ERROR
} from '../constants/ActionTypes'

const INITIAL_STATE = {
	list: 			{ items: [], error: null, loading: null },
	active: 		{ item: null, error: null, loading: null },
	new: 			{ item: null, error: null, loading: null },
	updated: 		{ item: null, error: null, loading: null },
	deleted: 		{ item: null, error: null, loading: null },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_USUARIOS:
		return { ...state, list: { items: [], error: null, loading: null } }

	case FETCH_USUARIOS_LOADING:
		return { ...state, list: { items: [], error: null, loading: true } }

	case FETCH_USUARIOS_SUCCESS:
		return { ...state, list: { items: action.payload.data, error: null, loading: false } }

	case FETCH_USUARIOS_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, list: { items: [], error: error, loading: false } }


	case FETCH_USUARIO:
		return { ...state, active: { item: null, error: null, loading: null } }

	case FETCH_USUARIO_LOADING:
		return { ...state, active: { item: null, error: null, loading: true } }

	case FETCH_USUARIO_SUCCESS:
		return { ...state, active: { item: action.payload.data, error: null, loading: false } }

	case FETCH_USUARIO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, active: { item: null, error: error, loading: false } }


	case CREATE_USUARIO:
		return { ...state, new: { item: null, error: null, loading: null } }

	case CREATE_USUARIO_LOADING:
		return { ...state, new: { item: null, error: null, loading: true } }

	case CREATE_USUARIO_SUCCESS:
		return { ...state, new: { item: action.payload.data, error: null, loading: false } }

	case CREATE_USUARIO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, new: { item: null, error: error, loading: false } }


	case UPDATE_USUARIO:
		return { ...state, updated: { item: null, error: null, loading: null } }

	case UPDATE_USUARIO_LOADING:
		return { ...state, updated: { item: null, error: null, loading: true } }

	case UPDATE_USUARIO_SUCCESS:
		return { ...state, updated: { item: action.payload.data, error: null, loading: false } }

	case UPDATE_USUARIO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, updated: { item: null, error: error, loading: false } }


	case DELETE_USUARIO:
		return { ...state, deleted: { item: null, error: null, loading: null } }

	case DELETE_USUARIO_LOADING:
		return { ...state, deleted: { item: null, error: null, loading: true } }

	case DELETE_USUARIO_SUCCESS:
		return { ...state, deleted: { item: action.payload.data, error: null, loading: false } }

	case DELETE_USUARIO_ERROR:
		error = action.payload.data || { message: action.payload.message }
		return { ...state, deleted: { item: null, error: error, loading: false } }

	default:
		return state;
  }
}
