import { FETCH_GLOBALS, RESET_GLOBALS } from '../constants/ActionTypes'

const INITIAL_STATE = {
	globals: []
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
	case FETCH_GLOBALS:
		return { ...state, globals: action.payload }

	case RESET_GLOBALS:
		return { ...state, globals: [] }

	default:
		return state;
  }
}
