const path = require('path')
const webpack = require('webpack')


module.exports = function (env) {
	return {
		entry: {
			'app': './src/index.js'
		},

		output: {
			filename: '[name].bundle.js',
			path: path.resolve(__dirname, './dist'),
			publicPath: '/',
			sourceMapFilename: '[name].map'
		},

		module: {
			rules: [
				{
					test: /\.(js|jsx)?$/,
					exclude: [
						path.resolve(__dirname, './node_modules')
					],
					loader: 'babel-loader'
				},
				{
					test: /\.(js)?$/,
					exclude: [
						path.resolve(__dirname, './node_modules')
					],
					include: [
						path.resolve(__dirname, './src/libs')
					],
					loader: 'script-loader'
				},
				{
					test:   /\.css?$/,
					use: [
						'style-loader',
						'css-loader'
					]
				},
				{
					test:   /\.scss?$/,
					use: [
						'style-loader',
						'css-loader',
						'sass-loader'
					]
				},
				{
					test: /\.(png|jpg|jpeg|svg|swoff|woff|woff2|eot|ttf|otf)?$/,
					use: { loader: 'url-loader', options: { limit: 100000 } }
				},
				{ 
					test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					use: { loader: 'url-loader', options: { limit: 100000, mimetype: 'application/font-woff' } }
				},
				{ 
					test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					loader: 'file-loader'
				}
			]
		},

		plugins: [
			new webpack.DefinePlugin({
				VERSION: JSON.stringify('1.0.0')
			})
		]
	}
}

