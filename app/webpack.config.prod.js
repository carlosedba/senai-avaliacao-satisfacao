const path = require('path')
const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')
const merge = require('webpack-merge')

const base = require('./webpack.config.base')

module.exports = function (env) {
	return merge(base(), {
		devtool: false,

		plugins: [
			new webpack.DefinePlugin({
				PRODUCTION: true,
			}),

			new webpack.EnvironmentPlugin({
				NODE_ENV: 'production'
			}),

			new webpack.optimize.UglifyJsPlugin({
				beautify: false,

				mangle: {
					screw_ie8: true,
					keep_fnames: true
				},

				compress: {
					screw_ie8: true
				},

				comments: false,
			}),

			new CompressionPlugin({
				asset: '[path].gz[query]',
				algorithm: 'gzip',
				test: /\.(js|html)?$/,
				threshold: 0,
				minRatio: 0.8
			})
		]
	})
}

