const path = require('path')
const webpack = require('webpack')
const merge = require('webpack-merge')

const base = require('./webpack.config.base')


module.exports = function (env) {
	return merge(base(), {
		devtool: 'cheap-module-eval-source-map',

		entry: [
			'react-hot-loader/patch',
			'webpack-dev-server/client?http://localhost:3000',
			'webpack/hot/only-dev-server',
			'./src/index.js'
		],

		output: {
			filename: 'app.bundle.js',
			sourceMapFilename: 'app.map'
		},

		devServer: {
			hot: true,
			contentBase: path.join(__dirname, 'dist'),
			publicPath: '/',
			compress: true,
			port: 3000,
			historyApiFallback: true
		},

		plugins: [
			new webpack.HotModuleReplacementPlugin(),

			new webpack.NamedModulesPlugin(),

			new webpack.DefinePlugin({
				PRODUCTION: false
			}),

			new webpack.EnvironmentPlugin({
				NODE_ENV: 'development'
			}),

			new webpack.optimize.UglifyJsPlugin({
				beautify: true,

				mangle: {
					screw_ie8: true,
					keep_fnames: true
				},

				compress: {
					screw_ie8: true
				},

				comments: true,

				sourceMap: this.devtool && (this.devtool.indexOf('sourcemap') >= 0 || this.devtool.indexOf('source-map') >= 0)
			}),
		]
	})
}

